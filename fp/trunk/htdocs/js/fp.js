
//-------- UI std functions ---------


var checkedall=0;

function menupost(op,table) {
  var url,tmp;
  url="?ctf_op="+op;
  if (table) url+="&ctf_table="+table;
  if (($('#ctf_op').val()=='csearch' || $('#ctf_op').val()=='viewrec')
      && ($('#ctf_op_args').val()) ) {
    url+="&ctf_op_args="+$('#ctf_op_args').val();
    //console.log('csearch');
    //console.log($('#ctf_op_args').val())
    /*
    try {
      tmp=JSON.parse($('#ctf_op_args').val());
      if tmp. 
    } catch () {
    }      
    */
  }
  location.href = url;
}  

function init_input_fields() {
$("input").each(function(){
    var val,oval;
    oval=$(this).attr('ovalue');  
    if (oval!=undefined) {
      val=$(this).attr('value'); 
      //alert(oval);
      if (oval=="True" && val=="True") {
        $(this).attr('checked', true);
      }  
    }
  });  
}



function setfilterselectors() {
  $("select.filterselect").each(function(){
    var sel,val,opts,i;
    sel=this;
    val=$(this).attr('setvalue');  
    if (val!=undefined) {
      opts=sel.options;
      for (i=0;i<opts.length;i++) {       
        if (opts[i].value==val) {
          sel.selectedIndex=i;
          break;
        }
      }
    }
  });  
}

function clearfilters() {
  $("select.filterselect").each(function(){
    this.selectedIndex=0;  
  }); 
  $("select.filterselectvalue").each(function(){
    this.selectedIndex=0;  
  }); 
  $("input.filterfld").each(function(){
    this.value='';  
  }); 
  $('#ctf_rowstart').val(0);
}

function sortcol(col) {
  //$('#ctf_op').val('list');
  $('#ctf_sort_field').val(col);
  sdir=$('#ctf_sort_dir').val();
  if (sdir==null || sdir=='' || sdir=='desc') {
    sdir='asc';
  } else if (sdir=='asc') {
    sdir='desc';
  }
  $('#ctf_sort_dir').val(sdir);   
  $('#ctf_rowstart').val(0);
  document.mainform.submit();
}

function clickrow(id) {
  $('#ctf_rowid').val(id);   
  $('#ctf_op').val('viewrec');
  if ($('#ctf_table')=="objects") document.mainform.method="get";
  document.mainform.submit();
}

function obj_click(id) {
  $('#ctf_rowid').val(id);   
  $('#ctf_op').val('viewrec');
  if ($('#ctf_table').val()=="objects" || $('#ctf_table').val()=="sobjects") 
    document.mainform.method="get";
  //console.log($('#ctf_table').val());
  document.mainform.submit();
}

function ext_obj_click(id) {
  clearfilters();
  clear_csearch_args();
  $('#ctf_rowid').val(id);   
  $('#ctf_op').val('viewrec');
  $('#ctf_table').val('objects');
  document.mainform.method="get";
  //if ($('#ctf_table').val()=="objects" || $('#ctf_table').val()=="sobjects") 
  //  document.mainform.method="get";
  //console.log($('#ctf_table').val());
  //console.log('extobjclick '+id);
  document.mainform.submit();
}

function file_click(id) {
  clearfilters();
  clear_csearch_args();
  $('#ctf_rowid').val(id);   
  $('#ctf_op').val('viewrec');
  $('#ctf_table').val('files');  
  document.mainform.method="get";
  //if ($('#ctf_table').val()=="objects" || $('#ctf_table').val()=="sobjects") 
  //  document.mainform.method="get";
  //console.log($('#ctf_table').val());
  document.mainform.submit();
}

function prev_rowid(curid) {
  var lst, i;
  lst=JSON.parse($("#ctf_list_ids_json").val());
  for(i=0;i<lst.length;i++) {
    if (lst[i]==curid) {
     if ((i-1)>=0) return lst[i-1];
      else return false;
    }
  }
  return false;
}

function next_rowid(curid) {
  var lst, el;
  lst=JSON.parse($("#ctf_list_ids_json").val());
  for(i=0;i<lst.length;i++) {
    if (lst[i]==curid) {
      if ((i+1)<lst.length) return lst[i+1];
      else return false;
    }
  }
  return false;
}

function selectallrows() {
  //console.log(checkedall);
  if (checkedall==1) {
    checkedall=0;
    $('#ctf_rowselectall').prop('checked', false); 
    $('input[name=ctf_rowselect]').prop('checked', false);
  } else {
    checkedall=1;
    $('#ctf_rowselectall').prop('checked', true); 
    $('input[name=ctf_rowselect]').prop('checked', true);
  }
}

function set_changed(fid) {
  bool_set(tid,fid); 
} 

function bool_set_changed(tid,fid) {
  bool_set(tid,fid); 
}  

function bool_set(tid,fid) {
  var pureid;
  pureid=tid.substr("spcfldt_".length);  // spcfldt_ prefix remove
  $('#label_'+pureid).css('color', 'red');
  $('#ischanged_'+pureid).val('True');   
  if ($('#'+tid).attr('checked')=='checked') { 
    $('#'+fid).attr('checked', false);
  } else {
    $('#'+fid).attr('checked', true);
  }    
}  


function nonbool_set_changed(tid) {
  $('#label_'+tid).css('color', 'red');  
  $('#ischanged_'+tid).val('True'); 
}  

function order_filled() {
  return $("#tmp_text").val()!="" && $("#contact_data").val()!="";
}

function confirmop(txt) {
  //console.log(txt);
  res=confirm(txt);
  return res;
}

function tgclick(item, dimension, event) {
  //alert(item);
  store_tagcloudsearch_args(item[0]);
  $("#mainform").submit();
}

function tghover(item, dimension, event) {
  event.target.style.cursor='pointer';
}

function tgcolor(word, weight, fontSize, distance, theta) {
  return "#eeeeee";
}

function store_ssearch_args() {
  var tmp;
  /*
  if (!$('#op_param_val').val()) {
     if (user_lang=='eng') bootbox.alert("Please enter the text to search for!");
     else bootbox.alert("Palun tipi otsitav tekst!");
     return false;
  }
  */
  tmp={val: $('#op_param_val').val(), 
       type: $('#op_param_type').val(),
       sort: $('#op_param_sort').val(),
       attach: $('#op_param_attach').prop('checked')}
  $('#ctf_op_args').val(encodeURIComponent(JSON.stringify(tmp)));
  $('#ctf_op').val('ssearch');
  return true;        
}       

function store_tagcloudsearch_args(tag) {
  var tmp;
  tmp={val: tag, 
       type: $('#op_param_type').val(),
       sort: $('#op_param_sort').val()}
  $('#ctf_op_args').val(encodeURIComponent(JSON.stringify(tmp)));
  $('#ctf_op').val('ssearch');
}

function store_csearch_args() {
  var tmp, i, el, lst;
  lst=["id","archive_id","author","title","keywords","place_of_publication","type","sort","extname","extval"];
  tmp={};
  found=false;  
  for (i=0; i<lst.length; i++) {
    el=lst[i];    
    if ($('#op_param_'+el).val()) {
      if (el!="sort" && el!="type") found=true;
      tmp[el]=$('#op_param_'+el).val();
    }
  }
  if ($('#op_param_attach').prop('checked')) tmp["attach"]=true;
  else tmp["attach"]=false;
  /*
  if (!found) {
     if (user_lang=='eng') bootbox.alert("Please enter the text to search for!");
     else bootbox.alert("Palun tipi otsitav tekst!");
     return false;
  }
  */
  //tmp["attach"]=$('#op_param_attach').val();
  $('#ctf_op_args').val(encodeURIComponent(JSON.stringify(tmp)));
  document.mainform.method="get";
  $('#ctf_op').val('csearch'); 
  return true;  
}  
       
function clear_csearch_args() {
  var i, el, lst;
  lst=["id","archive_id","author","title","keywords","place_of_publication","type","sort","extname","extval"];
  for (i=0; i<lst.length; i++) {
    el=lst[i];
    $('#op_param_'+el).val("");
  }
  $('#op_param_attach').prop('checked', false);
  $('#op_param_attach').removeAttr("selected");
  
}  


function store_ksearch_args() {
  var tmp, i, el, lst;
  if (!$('#op_param_keywords').val() || $('#op_param_keywords').val().length<4 ) {
    if (user_lang=='eng') bootbox.alert("Please enter at least four characters to search!");
    else bootbox.alert("Palun tipi vähemalt neljast tähest koosnev tekst!");
    return false;
  }  
  lst=["id","archive_id","author","title","keywords","place_of_publication","type","sort","extname","extval"];
  tmp={};
  for (i=0; i<lst.length; i++) {
    el=lst[i];
    if ($('#op_param_'+el).val()) {
      tmp[el]=$('#op_param_'+el).val();
    }
  }
  $('#ctf_op_args').val(encodeURIComponent(JSON.stringify(tmp)));
  document.mainform.method="get";
  $('#ctf_op').val('ksearch'); 
  return true;  
}  
       
function clear_ksearch_args() {
  var i, el, lst;
  lst=["id","archive_id","author","title","keywords","place_of_publication","type","sort","extname","extval"];
  for (i=0; i<lst.length; i++) {
    el=lst[i];
    $('#op_param_'+el).val("");
  }
}  

function search_keyword(kw) {
  tmp={"keywords":kw};
  $('#ctf_op_args').val(encodeURIComponent(JSON.stringify(tmp)));
  document.mainform.method="get";
  $('#ctf_op').val('csearch'); 
  document.mainform.submit();  
} 

function set_est_lang() {
  setCookie("lang","est",100); 
  document.mainform.submit();
}

function set_eng_lang() {
  setCookie("lang","eng",100);
  document.mainform.submit();
}

function setCookie(c_name,value,exdays) {
  var exdate=new Date();
  exdate.setDate(exdate.getDate() + exdays);
  var c_value=escape(value) + ((exdays==null) ? "" : "; expires="+exdate.toUTCString());
  document.cookie=c_name + "=" + c_value;
  //console.log(c_name + "=" + c_value);
}

function setobjtype(txt) {
  var tmp,ival,c;
  tmp=".views_"+txt;
  $(".views_hidden").each(function(i) { 
    //console.log(i)    
    ival=$(this).children("td.fldeditcol").children("input").val();
    //console.log(ival)
    if (!ival) {
      $(this).css("display","none");   
    } else {
      //console.log(ival);
    }      
  });      
  $(tmp).css("display","table-row");
  
  //  
  $(".viewnames_hidden").each(function(i) { 
    $(this).css("display","none"); 
  });   
  tmp=".viewname_"+txt;
  $(tmp).each(function(i) { 
    $(this).css("display","inline"); 
  });        
  
  //console.log("leitud: "+$("input[name=archive_id]").val());
  if ($("input[name=archive_id]").val()) return;  
  if (txt=="manuscript") c="K";  
  else if (txt=="printed") c="R";
  else if (txt=="journal") c="R";
  else if (txt=="article") c="R";
  else if (txt=="photo") c="F";
  else if (txt=="movie") c="Y";
  else if (txt=="audio") c="A";
  else if (txt=="slide") c="S";
  else if (txt=="physical") c="E";
  else c="X";  
  //console.log("call: "+"?ctf_op=next_archive_id&ctf_op_args="+c);
  $.ajax({
    url: "?ctf_op=next_archive_id&ctf_op_args="+c
  }).done(function (data) {  
    //console.log("data res: "+data);
    if (data && data!=0) $("input[name=archive_id]").val(data);
  });  
}

function setobj_visible_edit_fields() {
  var place,tmp,ival,c;
  if ($("#ctf_op").val()=="edit" && $("#ctf_table").val()=="objects" &&
      $("select[name='type']").val() ) {    
    tmp="tr.views_"+$("select[name='type']").val();
    $(tmp).each(function(i) {    
      ival=$(this).children("td.fldeditcol").children("input").val();
      if (!ival) {
        $(this).css("display","table-row");   
      }     
    }); 
  }   
}  

function keyword_search(txt) {
  var tmp, i, el, lst;
  tmp={"keywords":txt};
  $('#ctf_op_args').val(encodeURIComponent(JSON.stringify(tmp)));
  $('#ctf_op').val('csearch'); 
  document.mainform.method="get";
  document.mainform.submit();  
} 

function author_search(txt) {
  var tmp, i, el, lst;
  tmp={"author":txt};
  $('#ctf_op_args').val(encodeURIComponent(JSON.stringify(tmp)));
  $('#ctf_op').val('csearch'); 
  document.mainform.method="get";
  document.mainform.submit();  
} 

function embed_photo_page(surl,burl) {
  //alert(id+surl+burl);
  s="<textarea rows='6' cols='100' style='width: 500px; height: 100px;'>"
  s+="<a href='"+burl+"'><img src='"+surl+"'/></a></textarea>";
  if (user_lang=='eng') bootbox.alert("<h4>Please use this code snippet to add the picture to your web page:</h4>"+s);
  else bootbox.alert("<h4>Palun kasuta oma veebilehele lisamiseks seda koodijuppi:</h4>"+s);
  return false;
}

function embed_video_page(surl,mime) {
  //alert(id+surl+burl);
  s="<textarea rows='6' cols='100' style='width: 500px; height: 100px;'>"
  s+="<video width='640' height='480' controls><source src='"+surl+"' type='"+mime+"'></video></textarea>";
  if (user_lang=='eng') bootbox.alert("<h4>Please use this code snippet to add the picture to your web page:</h4>"+s);
  else bootbox.alert("<h4>Palun kasuta oma veebilehele lisamiseks seda koodijuppi:</h4>"+s);
  return false;
}

function hide_attach(id,rowid,nextop) {
  document.mainform.method="get";
  $('#ctf_op').val('hide_attach');
  $('#ctf_rowid').val(id);
  $('#ctf_nextop').val(nextop);
  $('#ctf_lastop').val(rowid);
  //console.log($('#ctf_op').val()+$(' #ctf_rowid').val());  
  document.mainform.submit();
}

function delete_comment(id,rowid,nextop) {
  document.mainform.method="get";
  $('#ctf_op').val('delete_comment');
  $('#ctf_rowid').val(id);
  $('#ctf_nextop').val(nextop);
  $('#ctf_lastop').val(rowid);
  //console.log($('#ctf_op').val()+$(' #ctf_rowid').val());  
  document.mainform.submit();
}

/* =============================

tagging stuff


============================ */

var tag_settings={
  align: { 'y': 'bottom' },
  offset: { 'top': -15 },
  'handlers': {
      'mouseenter': 'show',
      'mouseleave': 'hide'
  }
}

//var tag_y_size(id)

function show_tags(id) {    
  var $e,txt,offset;
  //console.log("show_tags: "+id);
  photoid=id;
  hide_tags(id);
  e='#photo_'+id;
  data=tag_data[String(id)];
  //console.log(e+" "+tag_settings+" "+data);
  $e = $(e);  
  $e.taggd(tag_settings);
  if (data) $e.taggd('items',data);   
  //$("#tagnamediv").show();
  $e.click(function(event) {
    $("#tagnamediv").show();
    offset = $(this).offset();
    //console.log("---");   
    $("#tagnamediv").offset({left:event.pageX-10,top:event.pageY-13});        
    //tagx=(event.pageX - offset.left - 21)/$e.width();
    //tagy=(event.pageY - offset.top - 20)/$e.height();          
    
    tagx=(event.pageX - offset.left - 10)/$e.width();
    tagy=(event.pageY - offset.top - 9)/$e.height();   
    
    //console.log($e.width()+" "+$e.height());
    //console.log("tagxyphotoid: "+tagx+" "+tagy+" "+photoid);    
  });
} 

function start_tags(id) {
  $("#start_tags_btn_"+id).css('display','none');
  $("#show_tags_btn_"+id).css('display','inline-block');
  $("#hide_tags_btn_"+id).css('display','inline-block'); 
  show_tags(id);  
}


function hide_tags(id) {    
  var $e,txt;
  e='#photo_'+id;
  data=tag_data[String(id)];
  //console.log("hiding"+e);
  $e = $(e);  
  $(".taggd-item").each(function(i,e) {
     $(e).hide();
  });  
  $("#tagnamediv").hide();
} 

function save_tag() {  
  var txt,ejs;
  if (!can_tag) {
    if (user_lang=='eng') bootbox.alert("Please log in to tag!");
    else bootbox.alert("Sildistamiseks eks pead sisse logima!");
    return;
  }
  txt=$("#tagname").val();
  //console.log("saving tag "+tagx+" "+tagy+" "+photoid+" "+txt);
  if (tag_data) {
    if (tag_data[String(photoid)]) tag_data[String(photoid)].push({'x':tagx, 'y':tagy, 'text':txt});  
    else tag_data[String(photoid)]=[{'x':tagx, 'y':tagy, 'text':txt}];    
  }  
  show_tags(photoid);
  $("#tagname").val("");
  $("#tagnamediv").hide();  
  ejs=encodeURIComponent(JSON.stringify({'x':tagx, 'y':tagy, 'text':txt, 'photoid': photoid}));
  $.ajax({
    url: "?ctf_op=save_tag&ctf_op_args="+ejs
  })
  /*
  .done(function ( data ) {
      console.log("save_tag ajax result: "+data);
  });
  */
  return false;
} 

function delete_tag(id,text) {  
  var newlst,i,sublst;
  //console.log("deleting tag "+id+" "+text);
  sublst=tag_data[String(photoid)];  
  //console.log("deleting tag "+id+" "+text+" "+String(photoid)+" "+String(tag_data));  
  newlst=[];
  for(i=0;i<sublst.length;i++) {
    if (id) {
      if (sublst[i].id!=id) newlst.push(sublst[i]);
    } else {
      if (sublst[i].text!=text) newlst.push(sublst[i]);
    }    
  }
  tag_data[String(photoid)]=newlst;
  show_tags(photoid);
  ejs=encodeURIComponent(JSON.stringify({'id':id, 'text':text, 'photoid': photoid}));
  $.ajax({
    url: "?ctf_op=delete_tag&ctf_op_args="+ejs
  })
  /*
  .done(function ( data ) {
      console.log("save_tag ajax result: "+data);
  });
  */
  return false;
} 


function photo_loaded(id) {
  
}

function preload(arrayOfImages) {
    $(arrayOfImages).each(function(){
        $('<img/>')[0].src = this;
        // Alternatively you could use:
        // (new Image()).src = this;
    });
}

preload([
  '/tlibrary/css/d.png'
]);

// ========== album stuff ===========

function save_album(album_id) {
  var keys;
  keys=[]
  $("input[name=ctf_rowselect]").each(function(e) {
    //console.log($(this).val());
    if ($(this).is(':checked')) {
      keys.push($(this).val());    
    }
  });
  $('#ctf_op').val("saveto_album");
  $('#ctf_rowid').val(album_id);
  $('#ctf_op_args').val(encodeURIComponent(JSON.stringify(keys)));
  document.mainform.submit();
}

// ========== maps stuff ===========


function openmap(lat,lng,edit) {
  if (lat!=0 && lng!=0) 
     window.open("/tlibrary/kaart.html?lat="+lat+"&lng="+lng+(edit==1 ? "&edit=1" : ""),'kaart');
  else window.open("/tlibrary/kaart.html?"+(edit==1 ? "edit=1" : ""),'kaart');  
}

function frommapclick(lat,lng) {
  //console.log("frommapclick called "+lat+" "+lng);
  $("input[name=lat]").val(lat);
  $("input[name=lng]").val(lng);  
}

// ======= logging in ======

/*
// Login in the current user via google or gacebook
function authUser(service) {
  //alert('authUser called');
  if (service="facebook")
    FB.login(checkFBLoginStatus, {scope:'email'});
  else if (service="google")  
    google_login();
  return false;  
}
*/

// ======= logging out =====

function logout() {
  if (login_service=="facebook") {        
    if (false) {
      //this version does real logoff from facebook
      FB.getLoginStatus(handleFBLogoutResponse);
    } else {      
      // this version does not log out of fb, just deletes cookie
      $('#logoutform').submit();
    }  
  } else if (login_service=="google") {    
    //google_disconnectUser();    
    $('#logoutform').submit();
  }  else {
    $('#logoutform').submit();
  }
  return false;
}

function handleFBLogoutResponse(response) {
  if (response.status === 'connected') {
    var uid = response.authResponse.userID;
    var accessToken = response.authResponse.accessToken;
    FB.logout(function(x) {
      //console.log('logged out');
      $('#logoutform').submit();
    });
  } else if (response.status === 'not_authorized') {
  } else {
  }  
}

function google_disconnectUser(access_token) {
  //console.log("revoking: "+access_token);
  var revokeUrl = 'https://accounts.google.com/o/oauth2/revoke?token=' + access_token;   
  $.ajax({
    type: 'GET',
    url: revokeUrl,
    async: false,
    contentType: "application/json",
    dataType: 'jsonp',
    success: function(nullResponse) {
      //console.log("user is disconnected");
    },
    error: function(e) {
      //console.log(e);
      // You could point users to manually disconnect if unsuccessful
      // https://plus.google.com/apps
    }
  });
}

function handleFBLogoutResponse(response) {
  if (response.status === 'connected') {
    var uid = response.authResponse.userID;
    var accessToken = response.authResponse.accessToken;
    FB.logout(function(x) {
      //console.log('logged out');
      $('#logoutform').submit();
    });
  } else if (response.status === 'not_authorized') {
  } else {
  }  
}


// ============== facebook stuff ============


// Login in the current user via Facebook and ask for email permission
function authUser(service) {
  //alert('authUser called');
  FB.login(checkFBLoginStatus, {scope:'email'});
}
  
// Check the result of the user status and display login button if necessary
function checkFBLoginStatus(response) {
  //alert('checkFBLoginStatus called');
  if(response && response.status == 'connected') {
    //console.log('User is authorized by js');  
    //console.log(response);
    //ask_fb_userinfo(response);
    //testAPI(response);
    submit_fb_access(response);
    //document.getElementById('fb_login_button').style.display = 'none';    
  } else {
    //console.log('User is not authorized');    
    //document.getElementById('loginButton').style.display = 'block';
  }
}  

function ask_fb_userinfo(response) {
  //console.log('Welcome from ask_fb_userinfo!');
  FB.api('/me', function(meresponse) {
    submit_fb_access(meresponse);  
  });
  /*
  console.log('status: ' + response.status);
  console.log('name: ' + response.name);
  console.log('userID: ' + response.authResponse.userID);    
  console.log('accessToken: ' + response.authResponse.accessToken);
  $('#loginform_op').val("fb_login")
  $('#loginform').submit()
  */
  
}

function submit_fb_access(response) {
  /*
  alert('submit_fb_access called');
  console.log('Welcome from submit_fb_access!');
  console.log('accessToken: '+response.authResponse.accessToken);
  console.log('response: ' + response);
  //console.log('name: ' + response.name);
  //console.log('id: ' + response.id);  
  console.log('userID: ' + response.authResponse.userID);    
  */
  if (response.authResponse.accessToken && response.authResponse.userID) {
    //alert('submit_fb_access called successfully'); 
    $('#loginform_op').val("fb_login")
    $('#loginform_auth_token').val(response.authResponse.accessToken);
    $('#loginform_uid').val(response.authResponse.userID);
    $('#loginform').submit();
  }
}


function testAPI(response) {
  /*
  console.log('Welcome!');
  console.log('status: ' + response.status);
  console.log('userID: ' + response.authResponse.userID);    
  console.log('accessToken: ' + response.authResponse.accessToken);
  console.log('Fetching your information.... ');
  FB.api('/me', function(meresponse) {
    console.log('Good to see you, ' + meresponse.name + '.');
    console.log(meresponse);    
  });
  */
}

function fbShare(topflag) {
  var link;
  link=top_link;
  note="";
  name="Eesti Pedagoogika Arhiiv-muuseum";  
  FB.ui({
      method: 'feed',
      display: 'popup',
      name: name,
      link: link,      
      caption: 'http://arhmus.tlu.ee',
      description: note
      },
    function(response) {
      if (response && response.post_id) {alert('Post was published.');} 
      else { alert('Post was not published.'); }
  });  
}



// ============== google stuff ============

var my_access_token;
var my_user_id;


function google_signinCallback(authResult) {
  //console.log("in google_signinCallback with login_service"+login_service);
  if (login_service=="google" || login_service=="facebook" || login_service=="password") return false;
  if (authResult['access_token'] && !authResult['error']) {
    // Successfully authorized    
    /*
    console.log('Authorized ok! ');
    console.log('authResult[access_token]: ' + authResult['access_token']);
    console.log('authResult[id_token]: ' + authResult['id_token']);
    console.log('authResult[email]: ' + authResult['email']);
    console.log('authResult[error]: ' + authResult['error']);    
    */ 
    my_user_id=authResult['id_token'];
    my_access_token=authResult['access_token'];
    getEmail(my_access_token);    
    // makeGoogleApiCall(my_access_token); // alternative to get social data
    return false;    
  } else if (authResult['error']) {
    // Possible error codes:
    //   "access_denied" - User denied access to your app
    //   "immediate_failed" - Could not automatially log in the user
    //console.log('There was an error: ' + authResult['error']);
  }
}

function getEmail(access_token){
  // email plus username plus id
  // Load the oauth2 libraries to enable the userinfo methods.
  //console.log("getEmail() called");
  gapi.client.load('oauth2', 'v2', function() {
    gapi.client.oauth2.userinfo.get().execute(function(resp) {
      /*
      console.log("resp: "+resp);
      console.log("resp.email: "+resp.email);      
      console.log("displayName: "+resp.name);
      console.log("id: "+resp.id);
      */
      $('#loginform_op').val("google_login");
      $('#loginform_auth_token').val(access_token);
      $('#loginform_uid').val(resp.id);
      if (resp.email) $('#loginform_email').val(resp.email);
      if (resp.name) $('#loginform_screenname').val(resp.name);
      $('#loginform_uid').val(resp.id);
      $('#loginform').submit();
      
      return false;
    });
  });
}

function makeGoogleApiCall(access_token) {
  // no email in this way
  gapi.client.load('plus', 'v1', function() {
    var request = gapi.client.plus.people.get({
      'userId': 'me'
    });
    request.execute(function(resp) {
      /*
      console.log("resp: "+resp);
      console.log("displayName: "+resp.displayName);
      console.log("id: "+resp.id);
      */
      $('#loginform_op').val("google_login");
      $('#loginform_auth_token').val(access_token);
      if (resp.displayName) $('#loginform_screenname').val(resp.displayName);
      $('#loginform_uid').val(resp.id);
      $('#loginform').submit();
      
      return false;
    });
  });
}



function getData(){
  //console.log("getData called");
  $.ajax({
    type: 'GET',
    url: 'https://www.googleapis.com/oauth2/v1/userinfo?alt=json&access_token='+my_access_token,
    async: true,
    contentType: "application/json",
    dataType: 'jsonp',
    success: function(p) {
      // Do something now that user is disconnected
      // The response is always undefined.
      /*
      console.log("success: "+p);
      console.log("success id: "+p.id);
      console.log("success data: "+p.data);
      console.log("success error: "+p.error);
      */
    },
    error: function(e) {
      // Handle the error
      //console.log("error: "+e);
      // You could point users to manually disconnect if unsuccessful
      // https://plus.google.com/apps
    }
  });
}


function toggleElement(id) {
  var el = document.getElementById(id);
  if (el.getAttribute('class') == 'hide') {
    el.setAttribute('class', 'show');
  } else {
    el.setAttribute('class', 'hide');
  }
}


function gplusShare(topflag) {
  var gurl="https://plus.google.com/share?url=";  
  link=top_link;
  gurl+=encodeURIComponent(link);
  //debug(gurl);
  window.open(gurl,'', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600');
}

function printOrder() {
  var txt = $("textarea[name='tmp_text']").text();
  //var oldPage = document.body.innerHTML;

  var w = window.open();
  $(w.document.body).html("<pre>"+txt+"</pre>");
  return false;
}



/* =================== admin ================== */

function admin() {
  /*
  $("#ctf_op").value();
  ?ctf_op=list&ctf_table=dobjects
  $("#mainform").submit();
  */
}

/* =================== admin ================== */


function liststats(pid) {
  $('#ctf_rowid').val(pid);
  $("#ctf_op").val("profile_stats");
  $("#mainform").submit();
}

function listpersons(pid) {
  $('#ctf_rowid').val(pid);
  $("#ctf_op").val("profile_persons");
  $("#mainform").submit();
}

function listexport(pid) {
  $('#ctf_rowid').val(pid);
  $("#ctf_op").val("profile_export");
  $("#mainform").submit();
}
function listshare(pid) {
  $('#ctf_rowid').val(pid);
  $("#ctf_op").val("profile_share");
  $("#mainform").submit();
}

function prow(pid) {
  /*
  $('#ctf_rowid').val(pid);
  $("#ctf_op").val("profile_person_view");
  $("#ctf_mainform").submit();
  */
}