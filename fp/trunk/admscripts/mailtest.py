#!/usr/bin/python
# -*- coding: utf-8 -*-

# Import smtplib for the actual sending function
import smtplib

# Import the email modules we'll need
from email.mime.text import MIMEText

me='tanel.tammet@gmail.com'
you='tanel.tammet@ttu.ee'
contents='My testmessage\nis here.\r\nThird line\r\nand fourth.'
subject='Testmessage'

# Create a text/plain message
msg = MIMEText(contents)

# me == the sender's email address
# you == the recipient's email address
msg['Subject'] = subject
msg['From'] = me
msg['To'] = you

# Send the message via our own SMTP server, but don't include the
# envelope header.
s = smtplib.SMTP('localhost')
s.sendmail(me, [you], msg.as_string())
s.quit()
