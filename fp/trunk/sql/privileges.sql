
-- tables

grant all privileges on users to fpuser;
grant all privileges on sessions to fpuser;
grant all privileges on settings to fpuser;
grant all privileges on profiles to fpuser;
grant all privileges on persons to fpuser;
grant all privileges on profile_sets to fpuser;

-- sequences

grant all privileges on users_id_seq to fpuser;
grant all privileges on users_id_seq to fpuser;
grant all privileges on sessions_id_seq to fpuser;
grant all privileges on settings_id_seq to fpuser;
grant all privileges on profiles_id_seq to fpuser;
grant all privileges on persons_id_seq to fpuser;
grant all privileges on profile_sets_id_seq to fpuser;
