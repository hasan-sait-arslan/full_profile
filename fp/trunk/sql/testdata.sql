delete from settings;
delete from users;

insert into users(id,username,password,firstname,lastname,email,status,level) values 
  (1,'tanel.tammet@gmail.com','62ad9af1ad48c7ddb8fed2975cfb6b44feac4197','Tanel','Tammet',
  'tanel.tammet@gmail.com','A',0);

insert into settings(id,name,value) values (1,'map_center_lat','58.3788');
insert into settings(id,name,value) values (2,'map_center_long','26.7202');
insert into settings(id,name,value) values (3,'csv_delimiter',';');
