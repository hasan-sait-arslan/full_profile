
-- drop indexes 

drop index users_username_idx;
drop index sessions_sid_idx;

-- create all indexes except for auto-generated primary keys and unique keys

create index users_username_idx on users (username);
create index sessions_sid_idx on sessions (sid);
