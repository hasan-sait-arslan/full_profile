# rebuilding and re-populating the whole database

# should be run as a user postgres
# user and database should have been created before, see createdb.sh

./createschema.sh
./privileges.sh
./instestdata.sh
./keywords.sh

