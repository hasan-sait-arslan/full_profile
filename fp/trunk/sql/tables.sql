--- database tables

--- drop all tables

drop table users cascade;
drop table sessions cascade;
drop table settings cascade;
drop table persons cascade;
drop table profiles cascade;
drop table profile_sets cascade;

--- drop all sequences

drop sequence users_id_seq;
drop sequence clients_id_seq;
drop sequence clients_users_id;
drop sequence sessions_id_seq;
drop sequence settings_id_seq;
drop sequence persons_id_seq;
drop sequence profiles_id_seq;
drop sequence profile_sets_id_seq;

--- create sequences

create sequence users_id_seq start with 1000;
create sequence clients_id_seq start with 1000;
create sequence client_users_id_seq start with 1000;
create sequence sessions_id_seq start with 1000;
create sequence settings_id_seq start with 1000;
create sequence persons_id_seq start with 1000;
create sequence profiles_id_seq start with 1000;
create sequence profile_sets_id_seq start with 1000;

--- create tables

create table users (
  id integer primary key default nextval('users_id_seq'), 
  username varchar(100) not null unique, -- std username or facebook:0013230 or google:233320111
  service varchar(20), -- null or facebook or google
  service_uid varchar(50), -- null or user id in facebook or google
  password varchar(256), -- not used for facebook or google case
  firstname varchar(100),
  lastname varchar(100),
  fullname varchar(100), -- used for facebook and google
  phone varchar(50),
  email varchar(100),
  address varchar(200),
  remarks varchar(200),
  level integer default 0,    --- 0 superuser, 1 admin, 2 data write/change, 3 facebook login, 10 unauthenticated  
  status char(1) default 'A', --- A: active, D: deactivated
  lang varchar(10),
  uistyle varchar(10),
  created_at timestamp default now(),
  updated_at timestamp default now(),
  updated_by varchar(100) -- username or systemname creating/updating
);

create table clients (
  id integer primary key default nextval('clients_id_seq'), 
  name varchar(100) not null unique, -- client name or just username like facebook:0013230 or google:233320111
  fullname varchar(100), 
  phone varchar(50),
  email varchar(100),
  address varchar(200),
  remarks varchar(200),
  status char(1) default 'A', --- A: active, D: deactivated
  created_at timestamp default now(),
  updated_at timestamp default now(),
  updated_by varchar(100) -- username or systemname creating/updating
);

create table client_users (
  id integer primary key default nextval('client_users_id_seq'), 
  client_id integer references clients(id) not null,
  user_id integer references users(id) not null,  
  user_role char(1) default 'A', --- admin, 
  status char(1) default 'A', --- A: active, D: deactivated
  created_at timestamp default now(),
  updated_at timestamp default now(),
  updated_by varchar(100) -- username or systemname creating/updating
);



create table sessions (
  id integer primary key default nextval('sessions_id_seq'),   
  sid varchar(200) not null unique,
  username varchar(100) references users(username) not null,
  endts timestamp not null default now()+interval '1 hour',
  token varchar(1000),
  ts timestamp default now()
);

create table settings (
  id integer primary key default nextval('settings_id_seq'),   
  name varchar(100) not null unique,
  value varchar(1000),
  created_at timestamp default now(),
  updated_at timestamp default now(),
  updated_by varchar(100) -- username or systemname creating/updating
);

 
create table profiles ( -- datasets analyzed
  id integer primary key default nextval('profiles_id_seq'), 
  name varchar(100) not null, -- name of the profile as shown in ui
  user_id integer references users(id) not null, -- user creating the profile
  infpath varchar(200), -- infile path 
  outfpath varchar(200), -- outfile path  
  status char(1) default 'W', -- W: waiting, P: processed, S: social, F: finished, D: deactivated (no shown with an object)
  created_at timestamp default now(),
  updated_at timestamp default now(),
  updated_by varchar(100) -- username or systemname creating/updating
);

create table profile_sets ( -- datasets analyzed
  id integer primary key default nextval('profile_sets_id_seq'), 
  name varchar(100) not null, -- name of the profile as shown in ui
  user_id integer references users(id) not null, -- user creating the profile set
  infpath varchar(200), -- infile path 
  outfpath varchar(200), -- outfile path  
  status char(1) default 'W', -- W: waiting, P: processed, S: social, F: finished, D: deactivated (no shown with an object)
  created_at timestamp default now(),
  updated_at timestamp default now(),
  updated_by varchar(100) -- username or systemname creating/updating
);

create table persons ( -- persons analyzed
  id integer primary key default nextval('persons_id_seq'),   
  name varchar(100) not null, -- name of the persion as show in ui
  user_id integer references users(id) not null, -- user creating the person
  
  profile_id integer references profiles(id) not null, -- the profile set in which the person is created in
  --- profile_set_id integer references profile_sets(id) not null, -- the profile set in which the person is created in
  
  firstname varchar(100) default 'unknown',
  middlename varchar(100) default 'unknown',
  lastname varchar(100) default 'unknown',
  e_mail varchar(100) default 'unknown',
  phone_number varchar(100) default 'unknown',
  location varchar(100) default 'unknown',
  sex char(1) default 'U', -- U unknown, M male, F female
  jsondata varchar(1000),  -- details of the person in json
  status char(1) default 'W', -- W: waiting, P: processed, F: finished, D: deactivated (no shown with an object)
  created_at timestamp default now(),
  updated_at timestamp default now(),
  updated_by varchar(100) -- username or systemname creating/updating
);




 
 

