#!/usr/bin/env python
#
# Google search (cached in whitedb)

import urllib
import json
import random
import time


GSAPI_URL="https://ajax.googleapis.com/ajax/services/search/web?v=1.0"
USERIP="193.40.252.80"

def gsapi_uri(q, start):
    return "".join([GSAPI_URL,
        "&rsz=8"
        "&start=",
        str(start),
        "&q=",
        urllib.quote(q),
        "&userip=",
        USERIP])

#
# Run a google search query
# returns a list of matching urls
#
def gsapi_call(q, start):
    request = urllib.urlopen(gsapi_uri(q, start))
    try:
        result = json.load(request)
        if result['responseStatus'] == 200:
            resrows = result['responseData']['results']
        else:
            resrows = []
    except:
        return None

    urls = []
    for row in resrows:
        if row.get('fileFormat') is not None:
            continue
        url = [row.get('url'),
            row.get('titleNoFormatting') or u'',
            row.get('content') or u'']
        if url[0]:
            urls.append(tuple([x.encode('utf-8') for x in url]))

    return urls

#
# Iterate over google search results
#
def gsapi_getpages(q, top=10):
    start = 0
    urls = []
    while start < top:
        needpages = min(8, top - start)
        if start > 0:
            time.sleep(random.randint(1,6)/3.0) #throttle down
        res = gsapi_call(q, start)
        if res is None:
            break
        for idx, url in enumerate(res[:needpages]):
            urls.append(url + (start + idx,))
        if len(res) < needpages:
            break
        start += needpages
    return urls

