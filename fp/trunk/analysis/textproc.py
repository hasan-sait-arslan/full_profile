#!/usr/bin/env python2
#
# Miscellaneous text processing functions shared by multiple modules

import re
import string
import urlparse

from stopwords import not_stopword

#
# Split page data and filter
#
def split_text(text):
    # filter html keywords
    text = re.sub(r'\<script([^\<]|\<[^/])+\</script[^\>]*\>', ' ', text)
    text = re.sub(r'\<style([^\<]|\<[^/])+\</style[^\>]*\>', ' ', text)
    plaintext = re.sub(r'\<[^\>]+\>', ' ', text)
    plaintext = re.sub(r'[\t\n\r\f\v\.,:;!\?"\'\(\)]', ' ', plaintext)
    return [w for w in plaintext.split(' ') if not_stopword(w)]

#
# Find all search terms in the word list. Record
# their positions in an array.
# poslist contains positions of grouped keywords (definite names)
# wpostlist contains positions of case-insensitive keywords (may be names)
#
def find_kw(kw, words):
    kwparts = kw.split(' ')
    kwlower = [k.lower() for k in kwparts]
    l = len(words)
    poslist, wposlist = [], []
    for i in xrange(l):
        if words[i].lower() in kwlower:
            wposlist.append(i)
        if words[i] in kwparts:
            match = False
            if i > 0 and words[i-1] in kwparts:
                poslist.append(i)
            elif i+1 < l and words[i+1] in kwparts:
                poslist.append(i)
    return poslist, wposlist

#
# Filter non-ASCII characters and punctuation from Unicode objects
#
def filt(unistr):
    isascii = set(string.letters + string.digits)
    return "".join([x for x in unistr if x in isascii])

#
# Compare ASCII parts of two byte strings (mostly names)
# Note that the strings are expected to be encoded in UTF-8. This is
# the case with the data returned from the web searches
#
# returns True if strings are the same, False otherwise
#
def same_letters(a, b):
    al = filt(a.decode('utf-8'))
    bl = filt(b.decode('utf-8'))
    return al == bl

#
# Compute intersection size of word lists, but use the
# stripped string format to kill weird encodings
#
def intersection_size(l1, l2):
    matches = 0
    for el1 in l1:
        for el2 in l2:
            if same_letters(el1, el2):
                matches += 1
    return matches

#
# Check if an URI originates from a given domain
#
def domain_matches(domain, url):
    hostname = urlparse.urlsplit(url).hostname
    return re.search(re.escape(domain) + '$', hostname) is not None

#
# Check if an URI has a specific path (matches one in a list)
# XXX: change this to a regex match so it's more generally useful
#
def path_matches(paths, url):
    path = urlparse.urlsplit(url).path
    for p in paths:
        if path.find(p) == 0:
            return True
    return False

