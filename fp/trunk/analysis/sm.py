#!/usr/bin/env python2
#
# Social Media networks

import textproc
from tagpers import combine

#
# Basic detector function
# all detector functions have the same prototype:
# arguments: url triple (url, title, description), wordlist, pagetext,
#   kw (to match), domain (to match), extkw
# returns confidence 0.0-1.0
#
# Basic detector validates the domain only
#
def basic_detector(url, words, pagetext, kw, domain, extkw):
    if textproc.domain_matches(domain, url[0]):
        return 0.6
    return 0.0

#
# Check for a page title typical of several social networks
# ( Name Othername thingie Networkname )
#
def title_check(title, kw, extkw, extjunk=0):
    titlewords = textproc.split_text(title)
    if len(titlewords) > 1:
        if textproc.same_letters(titlewords[-1], extkw):
            matches = textproc.intersection_size(
                kw.split(' '), titlewords[:-1])
            if matches > 1 and len(titlewords) < matches + 2 + extjunk:
                return 0.9
            elif matches > 0:
                return 0.4
    return 0.1

#
# Detect the Pinterest page
#
def pinterest_detector(url, words, pagetext, kw, domain, extkw):
    # step 1: filter pages NOT on Pinterest
    if not textproc.domain_matches(domain, url[0]):
        return 0.0
    # step 2: Pinterest personal pages have specific titles
    return title_check(url[1], kw, "Pinterest")

#
# Detect a Google plus page
# Currently same logic as Pinterest
# note that the page text is probably quite hard to parse, as its
# a mess of javascript
#
def gplus_detector(url, words, pagetext, kw, domain, extkw):
    if not textproc.domain_matches(domain, url[0]):
        return 0.0
    return title_check(url[1], kw, "Google+")

#
# Detect a Facebook page. These have nice descriptions as well.
#
def facebook_detector(url, words, pagetext, kw, domain, extkw):
    if not textproc.domain_matches(domain, url[0]):
        return 0.0
    titleconf = title_check(url[1], kw, "Facebook")
    descrconf = 0.1
    descrwords = textproc.split_text(url[2])
    poslist, dummy = textproc.find_kw(kw, descrwords)
    if poslist:
        if poslist[0] < 2:
            if url[2].find("is on Facebook") > 0:
                descrconf = 0.9
            else:
                descrconf = 0.7 # Possibly a customized page
        else:
            descrconf = 0.2  # refers to the person
    return combine([titleconf, descrconf])

#
# Detect a Youtube page. User pages have specific URL-s
#
def youtube_detector(url, words, pagetext, kw, domain, extkw):
    if not textproc.domain_matches(domain, url[0]):
        return 0.0
    titleconf = title_check(url[1], kw, "YouTube")
    if textproc.path_matches(["/user", "/channel"], url[0]):
        urlconf = 0.8
    else:
        urlconf = 0.1
    return combine([titleconf, urlconf])

#
# Detect a Flickr page. Check for a Flickr style title.
#
def flickr_detector(url, words, pagetext, kw, domain, extkw):
    if not textproc.domain_matches(domain, url[0]):
        return 0.0
    titlewords = textproc.split_text(url[1])
    if len(titlewords) > 1:
        if textproc.same_letters(titlewords[0], "Flickr") \
          and textproc.same_letters(titlewords[-1], "Photostream"):
            matches = textproc.intersection_size(
                kw.split(' '), titlewords[1:-1])
            if matches > 1 and len(titlewords) < matches + 3:
                return 0.9
            elif matches > 0:
                return 0.4
    return 0.1

#
# Detect a LinkedIn page
#
def linkedin_detector(url, words, pagetext, kw, domain, extkw):
    if not textproc.domain_matches(domain, url[0]):
        return 0.0
    return title_check(url[1], kw, "LinkedIn", 2)


networks = [
    ("site:www.facebook.com", "Facebook", "www.facebook.com", facebook_detector),
    ("youtube", "Youtube", "youtube.com", youtube_detector),
    ("twitter", "Twitter", "twitter.com", basic_detector),
    ("google+", "Google+", "plus.google.com", gplus_detector),
    ("linkedin", "LinkedIn", "linkedin.com", linkedin_detector),
    ("tumblr", "Tumblr", "tumblr.com", basic_detector),
    ("instagram", "Instagram", "instagram.com", basic_detector),
    ("flickr", "Flickr", "flickr.com", flickr_detector),
    ("pinterest", "Pinterest", "pinterest.com", pinterest_detector)
]

