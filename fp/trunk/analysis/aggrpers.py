#!/usr/bin/env python2
#
# Aggregate data per person

import whitedb
import sys
import math

import rowtype

PAGECOUNT_AMPLIFY = 15.0    # increase the page count up to this number
                            # if a word appears on fewer pages. float type.

# use a translation dictionary to treat similar words as one word
use_translate = False

if use_translate:
    from wordmerge import translate

#
# aggregate words per person
# return them as dict { person : { w : [( 4-tuple of stats)], ... }, ...  }
#
def pers_words(words_db, dict_db, search_id):
    c = words_db.cursor()
    c.execute(arglist = [(0, whitedb.wgdb.COND_EQUAL, rowtype.STATS),
                        (4, whitedb.wgdb.COND_EQUAL, search_id)])
    pw = {}
    for rec in c.fetchall():
        p = rec[1]
        if not pw.has_key(p):
            pw[p] = {}
        if use_translate:
                w = translate(dict_db, rec[5])
        else:
                w = rec[5]
        if not pw[p].has_key(w):
            pw[p][w] = []
        pw[p][w].append(tuple(rec)[6:] + (rec[2],))
    c.close()
    return pw

#
# Load re-aggregated data
#
def get_aggr(db, search_id):
    c = db.cursor()
    c.execute(arglist = [(0, whitedb.wgdb.COND_EQUAL, rowtype.AGGREGATE),
                        (2, whitedb.wgdb.COND_EQUAL, search_id)])
    pa = {}
    for rec in c.fetchall():
        p = rec[1]
        if not pa.has_key(p):
            pa[p] = {}
        pa[p][rec[3]] = (rec[4],rec[5],rec[6])
    c.close()
    return pa

#
# Store aggregate data for one word
#
def store_aggr(db, p, search_id, w, pc, bsd, asd):
    try:
        db.insert([rowtype.AGGREGATE, p, search_id, w, pc, bsd, asd])
    except:
        return -1, "Insert failed"
    return 0, "OK"

#
# Create a translation dictionary for merging keywords
#
def aggregate_pers(words_db, dict_db, tags_db, search_id):
    pw = pers_words(words_db, dict_db, search_id)
    for p, wd in pw.iteritems():
        # loop 1: count documents
        docs_seen = set()
        for w, tl in wd.iteritems():
            for ps in tl:
                docs_seen.add(ps[5])
        tpc = len(docs_seen)
        # loop 2: aggregate data
        for w, tl in wd.iteritems():
            pc = 0 # page count
            bsd = 0.0 # best strong distance
            asd = 0.0 # average strong distance
            for ps in tl:
                pc += 1
                bsd += ps[1]
                asd += ps[2]
            # adjust page count for low page counts
            pc = max(int(min(
                    math.ceil((pc*PAGECOUNT_AMPLIFY)/tpc),
                    PAGECOUNT_AMPLIFY)),
                pc)

            err, msg = store_aggr(tags_db, p, search_id, w, pc, bsd/pc, asd/pc)
            if err:
                return err, msg

    return 0, "OK"

def usage(prog):
    print "usage: %s <search id>"%(prog)
    sys.exit(2)

def die(msg):
    print msg
    sys.exit(1)

if __name__ == "__main__":
    if len(sys.argv) < 2:
        usage(sys.argv[0])

    try:
        search_id = int(sys.argv[1])
    except:
        die("Search id should be a number")

    try:
        words_db = whitedb.connect("102")
        if use_translate:
            dict_db = whitedb.connect("103")
        else:
            dict_db = None
        tags_db = whitedb.connect("104")
    except:
        die("Failed to attach to database")

    err, msg = aggregate_pers(words_db, dict_db, tags_db, search_id)

    if err:
        print "Error: "+msg
    else:
        print "OK"

    words_db.close()
    if dict_db is not None:
        dict_db.close()
    tags_db.close()
