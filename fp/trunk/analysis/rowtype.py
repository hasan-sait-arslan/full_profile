#!/usr/bin/env python2
#
# whitedb row type classifier
# all database rows are expected to have the type in column 0

CACHE=1
STATS=2
WORDCOUNT=3
SIMILAR=4
INDEX=5
DICTIONARY=6
AGGREGATE=7
TAG=8

