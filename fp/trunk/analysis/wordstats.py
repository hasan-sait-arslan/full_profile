#!/usr/bin/env python2
#
# Aggregate global stats for a word

import whitedb
import sys
import log

import rowtype


#
# Count all words
# return them as dict { word : count, ... }
#
def count_words(db, search_id):
    c = db.cursor()
    c.execute(arglist = [(4, whitedb.wgdb.COND_EQUAL, search_id)])
    wd = {}
    for rec in c.fetchall():
        if not wd.has_key(rec[5]):
            wd[rec[5]] = [rec[6], 1]
        else:
            wd[rec[5]][0] += rec[6]
            wd[rec[5]][1] += 1
    c.close()
    return wd

#
# Fetch words and counts from database
#
def get_count(db):
    c = db.cursor()
    c.execute(arglist = [(0, whitedb.wgdb.COND_EQUAL,
        rowtype.WORDCOUNT)])
    wd = {}
    for rec in c.fetchall():
        wd[rec[1]] = [rec[2], rec[3]]
    c.close()
    return wd

#
# Store count for one word
#
def store_wordcount(db, w, cnt, pages, extra):
    c = db.cursor()
    c.execute(arglist = [
        (0, whitedb.wgdb.COND_EQUAL, rowtype.WORDCOUNT),
        (1, whitedb.wgdb.COND_EQUAL, w)
        ])
    rec = c.fetchone()
    c.close()
    if rec:
        try:
            rec[2] += cnt
            rec[3] += pages
        except:
            return -1, "Update failed"
    else:
        try:
            db.insert([rowtype.WORDCOUNT, w, cnt, pages, extra])
        except:
            return -1, "Insert failed"
    return 0, "OK"

#
# Filter and count words
#
def aggregate_words(words_db, merged_db, search_id):
    # first pass: collect aggregate data (just counts)
    wd = count_words(words_db, search_id)
    c = 0
    for w, stats in wd.iteritems():
        if type(w) != type(" "):
            log.log("This cannot happen", log.ERR)
            w = str(w) # words should be strings
            extra = ">S"
        else:
            extra = None
        err, msg = store_wordcount(merged_db, w, stats[0], stats[1], extra)
        if err:
            return err, msg
        c += 1
    log.log("Collected " + str(c) + " words")
    return 0, "OK"


def usage(prog):
    print "usage: %s <search id>"%(prog)
    sys.exit(2)

def die(msg):
    print msg
    sys.exit(1)

if __name__ == "__main__":
    if len(sys.argv) < 2:
        usage(sys.argv[0])

    try:
        search_id = int(sys.argv[1])
    except:
        die("Search id should be a number")

    try:
        words_db = whitedb.connect("102")
        merged_db = whitedb.connect("103")
    except:
        die("Failed to attach to database")

    err, msg = aggregate_words(words_db, merged_db, search_id)

    if err:
        print "Error: "+msg
    else:
        print "OK"

    words_db.close()
    merged_db.close()
