# Configuration for the FP analysis engine

default = {
    "cache_db" : "101",
    "words_db" : "102",
    "stats_db" : "103",
    "tags_db" : "104",
    "msglog" : "fp_process.log",
    "errlog" : "fp_error.log",
    "watchdir" : "./2",
    "outdir" : "./3",
    "tags" : "concepts.json",
    "stats" : "classif.json",
    "mskey" : "mskey.txt",
    "origin_ip" : "193.40.252.80",
    "dl_threads" : 10,
    "search_limit" : 50,
    "extra_limit" : 10
}

def get_conf(filename):
    # ignore the file for now
    return default

