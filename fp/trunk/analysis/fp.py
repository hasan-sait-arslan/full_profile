#!/usr/bin/env python2
#
# Full Profile data analysis engine

import whitedb
import sys
import config
import time
import os

import log
import getdata
import wordstats
import aggrpers
import tagpers
import jsonout
# for config update
import bsearch
import gsearch
import searchcache

#
# Update the internal parameters from the loaded configuration
# XXX: This is fragile, better to supply the conf via the function call chain.
#
def set_params(conf):
    try:
        ff = open(conf["mskey"], "r")
        mskey = ff.readline().strip()
        ff.close()
        bsearch.bing_key = mskey
    except:
        pass
    try:
        gsearch.USERIP = conf["origin_ip"]
    except:
        pass
    try:
        getdata.dl_threads = conf["dl_threads"]
    except:
        pass
    try:
        searchcache.search_limit = conf["search_limit"]
    except:
        pass
    try:
        searchcache.extra_limit = conf["extra_limit"]
    except:
        pass

#
# Process a single search
#
def run_search(conf, search_id, cache_db, words_db, stats_db, tags_db, f):
    set_params(conf)
    err, msg = getdata.fetch_data(cache_db, words_db, tags_db, f, search_id)
    if not err:
        err, msg = wordstats.aggregate_words(words_db, stats_db, search_id)
    if not err:
        err, msg = aggrpers.aggregate_pers(words_db,
            stats_db, tags_db, search_id)
    if not err:
        try:
            # re-read tags file each time, so it can be updated while
            # the engine is running
            ff = open(conf["tags"], "r")
        except:
            ff = None
        if ff is None:
            err, msg = 1, "Failed to open tags file"
    if not err:
        try:
            # handle stats file like the tags file
            fff = open(conf["stats"], "r")
        except:
            fff = None
        if fff is not None:
            err, msg = tagpers.tag_pers(tags_db, ff, fff, search_id)
            ff.close()
            fff.close()
        else:
            err, msg = 1, "Failed to open stats file"
    if not err:
        try:
            ff = open(conf["outdir"]+"/"+str(search_id)+".json", "w")
        except:
            ff = None
        if ff is not None:
            err, msg = jsonout.json_out(tags_db, ff, search_id)
        else:
            err, msg = 1, "Failed to open JSON output file"
    return err, msg

#
# Main loop
#
def run_fp(conf, cache_db, words_db, stats_db, tags_db):
    while 1:
        wd = conf["watchdir"]+"/"
        l = os.listdir(wd)
        if len(l):
            ll = [ (wd+x, os.stat(wd+x).st_mtime) for x in l ]
            oldest = ll[0]
            for x in ll:
                if x[1] < oldest[1]:
                    oldest = x
            try:
                f = open(oldest[0], "r")
            except:
                log.log("Failed to open "+oldest[0], log.ERR)
                f = None                
            if f is not None:
                # use filename as search id (in case filename is an int)
                ptmp=oldest[0].rfind("/")
                if ptmp<0:ftmp=oldest[0]
                else: ftmp=oldest[0][ptmp+1:]
                try:
                  search_id = int(ftmp)
                except:
                  search_id = int(time.time())              
                err, msg = run_search(conf, search_id,
                    cache_db, words_db, stats_db, tags_db, f)
                f.close()
                if err:
                    log.log("Processing "+oldest[0]+" gave error: "+msg,
                        log.ERR)
                    break # exit for now
                os.unlink(oldest[0])
        time.sleep(10)
    return 0, "OK"

def usage(prog):
    print "usage: %s <config file>"%(prog)
    sys.exit(2)

def die(msg):
    print msg
    sys.exit(1)

if __name__ == "__main__":
    if len(sys.argv) < 2:
        usage(sys.argv[0])

    conf = config.get_conf(sys.argv[1])
    try:
        cache_db = whitedb.connect(conf["cache_db"])
        words_db = whitedb.connect(conf["words_db"])
        stats_db = whitedb.connect(conf["stats_db"])
        tags_db = whitedb.connect(conf["tags_db"])
    except:
        die("Failed to attach to database")

    try:
        if conf["msglog"] is not None:
            log.msglog = open(conf["msglog"], "a")
        if conf["errlog"] is not None:
            if conf["msglog"] != conf["errlog"]:
                log.errlog = open(conf["errlog"], "a")
            else:
                log.errlog = log.msglog
    except:
        die("Failed to initialize logfiles")

    err, msg = run_fp(conf, cache_db, words_db, stats_db, tags_db)
    if err:
        print "Error: "+msg
    else:
        print "Finished, no errors"

    tags_db.close()
    stats_db.close()
    words_db.close()
    cache_db.close()
