#!/usr/bin/env python2
#
# Generate the JSON output

import whitedb
import sys
import json

import rowtype

from tagpers import get_tags


#
# attach tags to persons
#
def json_out(tags_db, f, search_id):
    out = []
    pt = get_tags(tags_db, search_id)
    for p, ptags in pt.iteritems():
        out.append({"name" : p, "tags" : ptags})
    json.dump(out, f, ensure_ascii=True, indent=2)
    return 0, "OK"

def usage(prog):
    print "usage: %s <output file> <search id>"%(prog)
    sys.exit(2)

def die(msg):
    print msg
    sys.exit(1)

if __name__ == "__main__":
    if len(sys.argv) < 3:
        usage(sys.argv[0])

    try:
        search_id = int(sys.argv[2])
    except:
        die("Search id should be a number")

    try:
        tags_db = whitedb.connect("104")
    except:
        die("Failed to attach to database")

    try:
        f = open(sys.argv[1], "w")
    except:
        f = None
        err, msg = -1, "Cannot open output file"
    if f:
        err, msg = json_out(tags_db, f, search_id)
        f.close()

    if err:
        print "Error: "+msg
    else:
        print "OK"

    tags_db.close()
