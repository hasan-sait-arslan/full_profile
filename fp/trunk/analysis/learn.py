#!/usr/bin/env python2
#
# Naive Bayes classifier
import sys
import math
import json

EPSILON = 1e-20

#
# Statistics code derived from Python 3.4 statistics.py module
# which is under the following license:
#
# #  Copyright (c) 2013 Steven D'Aprano <steve+python@pearwood.info>.
# #
# #  Licensed under the Apache License, Version 2.0 (the "License");
# #  you may not use this file except in compliance with the License.
# #  You may obtain a copy of the License at
# #
# #  http://www.apache.org/licenses/LICENSE-2.0
#
def mean_and_variance(data):
    n = len(data)
    if n == 0:
        return 0, 0 # shouldn't happen
    elif n == 1:
        return data[0], 0.0

    mu = sum(data) / n  # use regular sum. We're expecting values with similar
                        # magnitude
    ss = sum((x - mu)**2 for x in data)
    # corrective wizardry from Python statistics.py. Leave it in.
    ss -= sum((x - mu) for x in data)**2 / n
    return mu, ss / (n - 1) # Why n-1 though?

#
# "Training" the classifier is essentially just computing statistics from
# samples.
#
# Input format:
# tuples (name (ignored), Tag, max pages, max weight, avg weight, total words,
# result (True/False))
#
# Output format (JSON compatible):
# dict { Tag: {"prior": prior probability of Tag being true,
#              "yes": { "mp" : (mean, variance),
#                       "maxw" : (mean, variance),
#                       "avgw" : (mean, variance),
#                       "words" : (mean, variance)},
#              "no":  { "mp" : (mean, variance),... }}}
#
# Unless the training samples are random, prior probabilities should be
# considered a placeholder value and their contents should be adjusted.
#
def train(samples):
    # group samples
    grouped = {}
    for sample in samples:
        tag = sample[1]
        if grouped.get(tag) is None:
            grouped[tag] = {True : [], False: []}
        grouped[tag][sample[6]].append(sample[2:6])

    def statsdict(smpl):
        return { "mp" : mean_and_variance([x[0] for x in smpl]),
                 "maxw" : mean_and_variance([x[1] for x in smpl]),
                 "avgw" : mean_and_variance([x[2] for x in smpl]),
                 "words" : mean_and_variance([x[3] for x in smpl]) }

    # compute statistics
    out = {}
    for tag, sd in grouped.iteritems():
        yl = len(sd[True])
        nl = len(sd[False])
        out[tag] = { "prior" : float(yl) / (yl+nl),
                     "yes" : statsdict(sd[True]),
                     "no" : statsdict(sd[False]) }
    return out

#
# Classify a sample
#
# Input sample format:
# tuple (name (ignored), Tag, max pages, max weight, avg weight, total words)
#
# Probability of the sample being related to tag, given a single parameter
# is computed by assuming a gaussian distribution of the parameters.
# The resulting probabilities are plugged into the "naive Bayes" classifier.
# (https://en.wikipedia.org/wiki/Naive_Bayes_classifier#Gaussian_naive_Bayes)
#
# Returns True if the sample is associated with the given tag,
# returns False otherwise.
#
def classify(tag, sample, stats):
    def p_attrib(x, mu, sigma):
        if mu < EPSILON or sigma < EPSILON:
            return 0.0
        return (1.0 / (math.sqrt(2 * math.pi * sigma))) * math.exp(
            -((x - mu)**2 / (2*sigma)))
    def stats_byattrib(stat):
        return [stat[k] for k in ["mp", "maxw", "avgw", "words"]]

    P = stats[tag]["prior"]
    NP = 1.0 - P
    ys = stats[tag]["yes"]
    ns = stats[tag]["no"]

    yvect = [p_attrib(z[0], z[1][0], z[1][1]) for z in zip(sample[2:6],
        stats_byattrib(ys))]
    nvect = [p_attrib(z[0], z[1][0], z[1][1]) for z in zip(sample[2:6],
        stats_byattrib(ns))]
    for pa in yvect:
        P *= pa
    for pa in nvect:
        NP *= pa
    return P >= NP # slight bias here

#
# Input samples are expected to be in CSV format
# name,Tag,max pages,max weight,avg weight,total words,1 or 0
#
def load_and_train(f):
    samples = []
    while 1:
        l = f.readline()
        if not l:
            break
        ll = l.strip().split(",")
        samples.append(ll[0:2] + map(float, ll[2:6]) + [int(ll[6])])
    return train(samples)

def usage(prog):
    print "usage: %s <samples file> <output json>"%(prog)
    sys.exit(2)

def die(msg):
    print msg
    sys.exit(1)

if __name__ == "__main__":
    if len(sys.argv) < 3:
        usage(sys.argv[0])

    try:
        f = open(sys.argv[1], "r")
    except:
        die("Failed to open input file")
    try:
        ff = open(sys.argv[2], "w")
    except:
        die("Failed to open output file")

    stats = load_and_train(f)
    json.dump(stats, ff, indent=2)

    f.close()
    ff.close()
