#!/usr/bin/env python
#
# Google search (cached in whitedb)

import whitedb
import random
import time
import log

import rowtype
from gsearch import gsapi_getpages
from bsearch import bing_websearch

search_limit = 50 # pages returned by the generic search
extra_limit = 10 # pages returned when targeting by keywords

#
# Store the result to a search
#
def cache_result(db, kw, extkw, urls):
    if not len(urls):
        db.insert([rowtype.CACHE, kw, extkw] + [None]*4) # empty result.
    else:
        for url in urls:
            db.insert([rowtype.CACHE, kw, extkw] + list(url))

#
# Check if the cache contains the search result
#
def find_cached_result(db, kw, extkw = None):
    c = db.cursor()
    argl = [(0, whitedb.wgdb.COND_EQUAL, rowtype.CACHE),
             (1, whitedb.wgdb.COND_EQUAL, kw)]
    if extkw is not None:
        argl.append((2, whitedb.wgdb.COND_EQUAL, extkw))
    c.execute(arglist=argl)
    urls = None # no results at all
    seen = set() # keep track of unique urls
    for rec in c.fetchall():
        if urls is None:
            urls = [] # present in cache (may be an empty result)
        if rec[3] != None and rec[3] not in seen:
            # cache 4-tuple (url, title, descr, rank)
            urls.append((rec[3], rec[4], rec[5], rec[6]))
            seen.add(rec[3])
    c.close()
    return urls

#
# Fetch the list urls matching to the keyword(kw)
# uses a database cache
#
def cached_web_search(db, kw, extkw=None, apis=["bing", "google"], burst=False):
    urls = find_cached_result(db, kw, extkw)
    if urls is None:
        if burst:
            time.sleep(random.randint(1,6)/3.0) #caller has asked for throttle
        search = kw
        if extkw is not None:
            search += " " + extkw
            top = extra_limit # targeted search, focus on primary results
        else:
            top = search_limit # non-targeted, try anything
        for api in apis:
            if api == "google":
                log.log("Not cached, getting from google: " + search)
                new_urls = gsapi_getpages(search, top)
                if new_urls is not None:
                    urls = new_urls
            elif api == "bing":
                log.log("Not cached, getting from bing: " + search)
                new_urls = bing_websearch(search, top)
                if new_urls is not None:
                    urls = new_urls
            if urls:
                break
        if urls is not None:
            log.log("Caching " + search + " " + str(len(urls)))
            cache_result(db, kw, extkw, urls)
    return urls

