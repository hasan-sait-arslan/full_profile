#!/usr/bin/env python2
#
# Fetch web pages through a cache

import urllib2
import time
import hashlib
import log
import os
import os.path
import socket
import gzip
from cStringIO import StringIO
import threading
import multiprocessing

PAGECACHE_DIR="1/"

#
# Initialize the thread pool for the cache
#
def initcache(worker_cnt=1):
    state = {
        "logl": threading.Lock(),
        "cachel": threading.Lock(),
        "in": multiprocessing.Queue(50),
        "out": multiprocessing.Queue(50),
        "shutdown": threading.Event()
    }
    workers = []
    for i in xrange(worker_cnt):
        workers.append(
            threading.Thread(target=queue_loop, args=(state,))
        )
    for t in workers:
        t.start()
    return (state, workers)

#
# Close the worker threads
#
def shutdown(cache):
    state, workers = cache
    state["shutdown"].set()
    for t in workers:
        t.join()

#
# Maintain the directory structure of the cache
#
def mkcachepath(doc_id):
    pref = doc_id[:2]
    suff = doc_id[2:]
    if not os.path.exists(PAGECACHE_DIR + pref):
        os.mkdir(PAGECACHE_DIR + pref)
    return PAGECACHE_DIR + pref + "/" + suff

#
# Retrieve webpage contents from a file.
#
def get_cached_page(doc_id):
    err = 0
    try:
        f = gzip.GzipFile(mkcachepath(doc_id), "r")
        #f = open(mkcachepath(doc_id), "r")
        pagetext = f.read()
        f.close()
    except:
        err = 1
        pagetext = ""
    return err, pagetext

#
# Store webpage contents to a file.
#
def store_cached_page(doc_id, pagetext):
    try:
        f = gzip.GzipFile(mkcachepath(doc_id), "w")
        #f = open(mkcachepath(doc_id), "w")
        f.write(pagetext)
        f.close()
    except:
        pass

#
# Try to determine if we can parse this mime type
#
def ishtml(request):
    try:
        ct = request.headers['content-type'].lower().strip()
        if ct.split(";")[0] == "text/html":
            return True
    except:
        return False
    return False

#
# Handle compressed content
#
def decode_pagetext(request):
    if request.headers.get('content-encoding') == 'gzip':
        log.log("uncompressing (gzip)")
        f = gzip.GzipFile(fileobj=StringIO(request.read()))
        pagetext = f.read()
    else:
        # assume plaintext
        pagetext = request.read()
    return pagetext

#
# Worker thread loop
#
def queue_loop(state):
    while True:
        try:
            url = state["in"].get(True, 2)
        except: # queue empty
            url = None
        if url is None:
            if state["shutdown"].is_set():
                break
            else:
                continue
        doc_id, pagetext = fetch_page(url[0], state["logl"], state["cachel"])
        state["out"].put((url, doc_id, pagetext))

#
# Fetch page data
#
def fetch_page(url, logl, cachel):
    doc_id = hashlib.md5(url).hexdigest()
    with cachel:
        err, pagetext = get_cached_page(doc_id)
    if err:
        with logl:
            log.log("not in cache: " + url)
        try:
            request = urllib2.Request(url, headers={"User-Agent":
                "Mozilla/5.0 (Windows NT 6.1; WOW64; rv:12.0) Gecko/20100101 Firefox/12.0",
                "Accept-encoding":"gzip"
            })
            request = urllib2.urlopen(request, timeout=12)
            if request.getcode() == 200 and ishtml(request):
                pagetext = decode_pagetext(request)
            else:
                with logl:
                    log.log("ignoring (not html or 404) " + url)
                pagetext = ""
            store_cached_page(doc_id, pagetext)
            # uncomment if incoming bandwidth needs to be shared
            #time.sleep(1)
        except socket.timeout as err:
            with logl:
                log.log("ignoring (timeout) " + url)
            with cachel:
                store_cached_page(doc_id, "")
        except urllib2.HTTPError as err:
            with logl:
                log.log("ignoring (%d %s) "%(err.code, err.reason) + url)
            with cachel:
                store_cached_page(doc_id, "")
            pagetext = ""
        except:
            import traceback
            with logl:
                log.log(traceback.format_exc())
            pagetext = ""
    return doc_id, pagetext

