#!/usr/bin/env python
#
# ConceptNet5 downloader.

import urllib2
import json
import time
import random

from pagecache import decode_pagetext


CNET5API_URL= "http://conceptnet5.media.mit.edu/data/5.2"
#BF = [50, 10, 5, 3, 1, 0]  # branching factor by depth (includes a terminator)
BF = [50, 10, 2, 1, 0]
#BF = [5, 3, 1, 0]

#
sleep_extension = 1   # n/3 seconds added to the average wait between calls
courtesy_wait = 12    # if we get a 429, wait before retrying

import urllib2

def cnet5_uri(concept, limit=50):
    return "".join([CNET5API_URL,
        "/c",      # concept
        "/en/",    # language
        urllib2.quote(concept),   # expecting valid input here
        "?limit=",
        str(limit)])

#
# Calculate a randomized pause
#
def randomized_pause():
    time.sleep(random.randint(1 + sleep_extension, 6 + sleep_extension) / 3.0)

#
# Handle a 429 Too Many Requests error
#
def handle_429():
    global courtesy_wait, sleep_extension
    if courtesy_wait > 1200:
        print "The server does not like us"
        return False
    print "Doing a courtesy wait of "+str(int(courtesy_wait))+" seconds"
    time.sleep(courtesy_wait)
    courtesy_wait *= 1.5
    sleep_extension += 1
    return True

#
# Download JSON from the ConceptNet5 server
#
def cnet5_call(concept, limit=50):
    try:
        request = urllib2.Request(cnet5_uri(concept, limit))
    except:
        return 404

    request.add_header("Accept-encoding", "gzip")
    try:
        r = urllib2.urlopen(request)
    except urllib2.HTTPError as err:
        if err.code == 429:
            return 429
        if err.code == 404:
            return 404
        else:
            return None

    code = r.getcode()
    if code == 404:
        return 404
    elif code != 200:
        return None
    try:
        result = json.loads(decode_pagetext(r))
        if result.has_key("edges"):
            edges = result["edges"]
        else:
            edges = []
    except:
        return None

    return edges

#
# Decide whether we consider this edge relevant
#
def relevant_edge(edge):
    w = edge.get("weight", 1.0)
    rel = edge.get("rel", "").replace("/r/", "")
    if w <= 0:
        return False
    elif not rel:
        return False
    elif rel[:3] == "Not":
        return False
    elif rel == "Antonym":
        return False
    else:
        return True

#
# Determine the related concept (it can be either start or end)
#
def extract_relation(concept, edge):
    w = edge.get("weight", 1.0)
    end = edge.get("end", "")
    if end[:6] == "/c/en/":
        endconcept = end.split("/")[-1]
        if endconcept != concept:
            return (endconcept,
                edge.get("endLemmas", endconcept.replace("_", " ")), w)
    start = edge.get("start", "")
    if start[:6] == "/c/en/":
        startconcept = start.split("/")[-1]
        if startconcept != concept:
            return (startconcept,
                edge.get("startLemmas", startconcept.replace("_", " ")), w)
    return None, None, None

#
# Start with the concept and collect relevant results
# Breadth first search (so we see the closer relations first)
# Returns err, tree
#
def fetch_tree(root_concept):
    related = [(root_concept, 0, None)]
    seen = set([root_concept])
    queue = [(0, root_concept)]

    while queue:
        depth, concept = queue.pop(0)
        max_children = BF[depth]
        if max_children > 0:
            randomized_pause() #throttle down
            while True:  # download with retries
                edges = cnet5_call(concept)
                if edges == 429:
                    if not handle_429():
                        return 1, related
                else:
                    break
            #print (edges,)
            if edges is None:
                return 2, related
            elif edges == 404:
                continue #skip
            cnt = 0
            for edge in edges:
                if relevant_edge(edge):
                    relconcept, plaintext, w = extract_relation(concept, edge)
                    if relconcept:
                        if relconcept in seen: # avoid cycles
                            continue
                        seen.add(relconcept)
                        #print (depth, relconcept, w)
                        parts = [x for x in plaintext.split(" ") if len(x) > 2]
                        for p in parts:
                            # allow multiword, weaken weight
                            related.append((p, depth+1, w / len(parts)))
                        queue.append((depth+1, relconcept))
                        cnt += 1
                        if cnt >= max_children:
                            break # seen enough

    return 0, related

#
# Process the list of tags in the input file
# Input format:
# Tag,concept,...
#
def fetch_tagdata(f):
    out = {}
    while 1:
        l = f.readline()
        if not l:
            break
        ll = [x.strip() for x in l.split(",")]
        tag, concepts = ll[0], ll[1:]
        out[tag] = []
        for concept in concepts:
            err, tree = fetch_tree(concept)
            out[tag] += tree
            print (tag, concept, len(tree))
            if err:
                print "Stopping with error code %d"%(err)
                return out

    return out

def usage(prog):
    print "usage: %s <concepts file> <output file>"%(prog)
    sys.exit(2)

def die(msg):
    print msg
    sys.exit(1)

if __name__ == "__main__":
    import sys
    if len(sys.argv) < 3:
        usage(sys.argv[0])

    try:
        f = open(sys.argv[1], "r")
    except:
        die("Failed to open input file")
    try:
        ff = open(sys.argv[2], "w")
    except:
        die("Failed to open output file")
    out = fetch_tagdata(f)
    json.dump(out, ff)

    f.close()
    ff.close()

