#!/usr/bin/python
# -*- coding: utf-8 -*-

import sys
from fpsite.soc_ctrl import ctrl_main

def main():
  conf='/home/tanel/Ms/fp/trunk/config/tanel.cfg'
  if len(sys.argv)<2:
    ctrl_main(conf,None)
  else:
    print "called ok with ",sys.argv[1]
    ctrl_main(conf,sys.argv[1])

main()
