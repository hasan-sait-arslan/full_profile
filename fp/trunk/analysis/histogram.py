#!/usr/bin/env python2
#
# Statistical distribution of word frequencies

import whitedb
import sys
import math

import rowtype
from wordstats import get_count

#
# Compute bucket boundaries (linear scale)
#
def mkbuckets(wd, buckets, mode=1):
    minf = 10000000
    maxf = 0
    c = 0
    # stats
    for w, stats in wd.iteritems():
        freq = stats[mode]
        if minf > freq:
            minf = freq
        if maxf < freq:
            maxf = freq
        c += 1

    bucket = {}
    if c < 2:
        bucket[maxf] = 0
        return c, bucket

    lmaxf = math.log(maxf)
    lminf = math.log(minf)
    step = (lmaxf - lminf) / buckets
    bound = lminf
    while bound < lmaxf:
        bound += step
        bucket[int(math.ceil(math.exp(bound)))] = 0

    return c, bucket

#
# Sort words between buckets
#
def fill_buckets(wd, bucket, mode=1):
    k = bucket.keys()
    k.sort()
    maxb = 0
    for w, stats in wd.iteritems():
        freq = stats[mode]
        for bound in k:
            if freq <= bound:
                bucket[bound] += 1
                if bucket[bound] > maxb:
                    maxb = bucket[bound]
                break
    return bucket, maxb

#
# Histogram output
#
def print_buckets(bucket, maxb):
    k = bucket.keys()
    k.sort()
    for bound in k:
        print bound, "\t", "#"*(int(40*bucket[bound]/maxb)), bucket[bound]

#
# Create the histogram
#
def mkhistogram(stats_db, buckets, mode=1):
    wd = get_count(stats_db)
    c, bucket = mkbuckets(wd, buckets, mode)
    if c < 2:
        print "Not enough data"
        return 0, "OK"
    bucket, maxb = fill_buckets(wd, bucket, mode)
    if maxb > 0:
        print_buckets(bucket, maxb)
    return 0, "OK"


def usage(prog):
    print "usage: %s count|pages <buckets>"%(prog)
    sys.exit(2)

def die(msg):
    print msg
    sys.exit(1)

if __name__ == "__main__":
    validarg = ["count", "pages"]
    if len(sys.argv) < 3 or sys.argv[1] not in validarg:
        usage(sys.argv[0])

    try:
        buckets = int(sys.argv[2])
    except:
        die("Bucket count should be a number")

    try:
        stats_db = whitedb.connect("103")
    except:
        die("Failed to attach to database")

    if sys.argv[1] == "count":
        err, msg = mkhistogram(stats_db, buckets, 0)
    elif sys.argv[1] == "pages":
        err, msg = mkhistogram(stats_db, buckets, 1)

    if err:
        print "Error: "+msg
    else:
        print "OK"

    stats_db.close()

