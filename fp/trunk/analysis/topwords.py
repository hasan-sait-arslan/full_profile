#!/usr/bin/env python2
#
# Query top words from the word database

import whitedb
import sys

import rowtype
from wordstats import get_count

#
# Output the word in an easily sortable format
# (pipe to | sort -n)
#
def print_word(w, count, pages):
    print pages, count, w

#
# Create the word list
#
def mkwordlist(stats_db, pages, count):
    c = stats_db.cursor()
    c.execute(arglist=[ (0, whitedb.wgdb.COND_EQUAL, rowtype.WORDCOUNT),
                        (2, whitedb.wgdb.COND_GTEQUAL, count),
                        (3, whitedb.wgdb.COND_GTEQUAL, pages)
                        ])
    rec = c.fetchone()
    while rec:
        print_word(rec[1], rec[2], rec[3])
        rec = c.fetchone()
    c.close()
    return 0, "OK"

def usage(prog):
    print "usage: %s <min pages> <min count>"%(prog)
    sys.exit(2)

def die(msg):
    print msg
    sys.exit(1)

if __name__ == "__main__":
    if len(sys.argv) < 3:
        usage(sys.argv[0])

    try:
        pages = int(sys.argv[1])
    except:
        die("Page limit should be a number")

    try:
        count = int(sys.argv[2])
    except:
        die("Count limit should be a number")

    try:
        stats_db = whitedb.connect("103")
    except:
        die("Failed to attach to database")

    err, msg = mkwordlist(stats_db, pages, count)

    if err:
        print "Error: "+msg

    stats_db.close()

