#!/usr/bin/env python2
#
# Extract words and their positions from Internet documents

import whitedb
import sys
import math

from searchcache import cached_web_search
from pagecache import initcache, shutdown
import rowtype
import log
import sm
from tagpers import store_tag
from textproc import domain_matches, split_text, find_kw


TITLE_DIST=5             # consider title words to be at this distance
DESCR_DIST=10            # and description words at this distance
RELEVANCE_THRESHOLD=0.5  # one or two matches need to be quite close (<10),
                         # five or more may be distant (>500)
SM_THRESHOLD=0.3         # don't add tags below this confidence level
dl_threads = 5           # a modest default; increase at will

#
# Compute the best distance to one of the numbers in the list
#
def distance(n, l):
    best = abs(n - l[0])
    for p in l[1:]:
        if best == 0:
            break # can't improve, return early
        d = abs(n - p)
        if d < best:
            best = d
    return best

#
# Word data aggregator helper
#
def add_wordstats(res, w, dist, wdist):
    if not res.has_key(w):
        return [1, dist, dist, wdist, wdist]
    else:
        prev = res[w]
        return [prev[0]+1,
            prev[1] > dist and dist or prev[1],
            prev[2]+dist,
            prev[3] > wdist and wdist or prev[3],
            prev[4]+wdist
        ]

#
# Extract a list of keywords from the page.
# They are returned as a list of tuples (keyword, count, ...other stats...)
#
def page_words(title, descr, text, kw):
    all_words = split_text(text)
    poslist, wposlist = find_kw(kw, all_words)
    if not poslist: # keywords not on page, relation could be too distant
        return []

    res = {}
    # title and url
    titlewords = split_text(title)
    for w in map(lambda x: x.lower(), titlewords):
        res[w] = add_wordstats(res, w, TITLE_DIST, TITLE_DIST)
    descrwords = split_text(descr)
    for w in map(lambda x: x.lower(), descrwords):
        res[w] = add_wordstats(res, w, DESCR_DIST, DESCR_DIST)

    for i in xrange(len(all_words)):
        dist = distance(i, poslist)
        wdist = distance(i, wposlist)
        if dist == 0 or wdist == 0:
            continue # ignore search terms themselves
        w = all_words[i].lower()
        res[w] = add_wordstats(res, w, dist, wdist)

    wordstats = []
    for w, stats in res.iteritems():
        avgdist = float(stats[2]) / stats[0]
        avgwdist = float(stats[4]) / stats[0]
        coeff = float(stats[0]) / math.log(stats[1] + 1, 2)
        if coeff > RELEVANCE_THRESHOLD:
            wordstats.append((w,
                stats[0],
                float(stats[1]),
                avgdist,
                float(stats[3]),
                avgwdist))
    return wordstats

#
# Store word statistics
# words array contains tuples (word, count, best distance, avg distance,
#   best weak distance, avg weak distance)
#
def store_words(db, kw, doc_id, search_id, words):
    for w in words:
        c = db.cursor()
        c.execute(arglist = [
            (0, whitedb.wgdb.COND_EQUAL, rowtype.STATS),
            (1, whitedb.wgdb.COND_EQUAL, kw),
            (2, whitedb.wgdb.COND_EQUAL, doc_id),
            (5, whitedb.wgdb.COND_EQUAL, w[0])
            ])
        rec = c.fetchone()
        c.close()
        if rec:
            try:
                rec[4] = search_id
                rec[6] = w[1]
                rec[7] = w[2]
                rec[8] = w[3]
                rec[9] = w[4]
                rec[10] = w[5]
            except:
                return -1, "Update failed"
        else:
            try:
                db.insert([rowtype.STATS,
                    kw, doc_id, search_id, search_id] + list(w))
            except:
                return -1, "Insert failed"
    return 0, "OK"


#
# Check if a domain is present in an iterable
#
def domain_seen(domain, urls):
    for url in urls:
        if domain_matches(domain, url[0]):
            return True
    return False

#
# Search discarding duplicate results. Uses external memory to store
# seen results.
#
def search_nodupes(cache_db, kw, extkw, burst, memory):
    urls = cached_web_search(cache_db, kw, extkw, burst=burst) or []
    new_urls = []
    for u in urls:
        if u[0] not in memory:
            memory.add(u[0])
            new_urls.append(u)
    return new_urls, memory


#
# Get a list of names from a file. Search the Internet for the keyword
# data about the names and store it in the database.
#
def fetch_data(cache_db, words_db, tags_db, f, search_id):
    burst = False
    cache = initcache(dl_threads)
    while 1:
        l = f.readline()
        if not l:
            break

        kwparts = [n.capitalize() for n in l.strip().split(' ') if n]
        if len(kwparts) < 2:
            continue # guard from hairy input

        # input file no longer has Lastname Firstname (Middlenames)
        #kw = " ".join(kwparts[1:] + kwparts[:1])
        kw = " ".join(kwparts)

        # step 1: non-targeted search
        urls, seen = search_nodupes(cache_db, kw, None, burst, set())

        # step 2: targeted searches
        # XXX: hardcode 'sport' for now
        burst = True
        new_urls, seen = search_nodupes(cache_db, kw, "sport", burst, seen)
        urls += new_urls

        found_networks = {}
        for extkw, tag, domain, detector in sm.networks:
            found_networks[tag] = []
            # search from SM domain anyway, correct sites may slip
            # between the cracks
            #if not domain_seen(domain, urls):
            new_urls, seen = search_nodupes(cache_db,
                kw, extkw, burst, seen)
            urls += new_urls

        # step 3: download documents, identify specific sites
        #for url in urls:
        #    doc_id, text = fetch_page(url[0])
        pc = len(urls)
        idx = 0
        done = 0
        while done < pc:
            # top up input queue first
            while idx < pc:
                try:
                    cache[0]["in"].put(urls[idx], False)
                    idx += 1
                except:
                    break # queue full for now
            try:
                url, doc_id, text = cache[0]["out"].get(True, 300)
                done += 1
            except:
                err, msg = -1, "Cache timed out"
                break

            words = page_words(url[1], url[2], text, kw)
            if len(words) > 0: # document is related
                for extkw, tag, domain, detector in sm.networks:
                    P = detector(url, words, text, kw, domain, extkw)
                    if P >= SM_THRESHOLD:
                        # log so we can check for false positives
                        with cache[0]["logl"]:
                            log.log(url[0] + " belongs to " + kw + " (%.2f)"%(P))
                        found_networks[tag].append(P)
            else:
                with cache[0]["logl"]:
                    # log so we can check for false negatives
                    log.log(url[0] + " is not relevant for " + kw)
            err, msg = store_words(words_db, kw, doc_id, search_id, words)
            if err:
                break

        if err:
            break

        # step 4: add social network tags based on best matches
        for tag, pvect in found_networks.iteritems():
            if pvect:
                P = max(pvect) # use the best match for now
                err, msg = store_tag(tags_db, kw, search_id, tag, P)
                if err:
                    break

    if cache is not None:
        shutdown(cache)
        log.log("Cache shutdown successful")
    if err:
        return err, msg
    return 0, "OK"

def usage(prog):
    print "usage: %s <names file> <search id>"%(prog)
    sys.exit(2)

def die(msg):
    print msg
    sys.exit(1)

if __name__ == "__main__":
    if len(sys.argv) < 3:
        usage(sys.argv[0])

    try:
        cache_db = whitedb.connect("101")
        words_db = whitedb.connect("102")
        tags_db = whitedb.connect("102")
    except:
        die("Failed to attach to database")

    try:
        f = open(sys.argv[1], "r")
    except:
        die("Failed to open input file")

    try:
        search_id = int(sys.argv[2])
    except:
        die("Search id should be a number")

    try:
        ff = open("mskey.txt", "r")
        mskey = ff.readline().strip()
        ff.close()
        import bsearch
        bsearch.bing_key = mskey
    except:
        pass

    err, msg = fetch_data(cache_db, words_db, tags_db, f, search_id)
    if err:
        print "Error: "+msg
    else:
        print "OK: "+msg

    f.close()
    tags_db.close()
    cache_db.close()
    words_db.close()

