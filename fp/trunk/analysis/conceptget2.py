#!/usr/bin/env python
#
# ConceptNet5 downloader.

import urllib2
import json
import time
import random

from pagecache import decode_pagetext


CNET5API_URL= "http://conceptnet5.media.mit.edu/data/5.2"
LANG="en"
#
sleep_extension = 1   # n/3 seconds added to the average wait between calls
courtesy_wait = 12    # if we get a 429, wait before retrying

import urllib2

def cnet5_uri(concept, limit=1000):
    return "".join([CNET5API_URL,
        "/assoc/list",            # concept
        "/%s/"%(LANG),            # language of the concept
        urllib2.quote(concept),   # expecting valid input here
        "?filter=/c/%s"%(LANG),   # language of the results
        "&limit=",
        str(limit)])

#
# Calculate a randomized pause
#
def randomized_pause():
    time.sleep(random.randint(1 + sleep_extension, 6 + sleep_extension) / 3.0)

#
# Handle a 429 Too Many Requests error
#
def handle_429():
    global courtesy_wait, sleep_extension
    if courtesy_wait > 1200:
        print "The server does not like us"
        return False
    print "Doing a courtesy wait of "+str(int(courtesy_wait))+" seconds"
    time.sleep(courtesy_wait)
    courtesy_wait *= 1.5
    sleep_extension += 1
    return True

#
# Download JSON from the ConceptNet5 server
#
def cnet5_call(concept, limit=1000):
    try:
        request = urllib2.Request(cnet5_uri(concept, limit))
    except:
        return 404

    request.add_header("Accept-encoding", "gzip")
    try:
        r = urllib2.urlopen(request)
    except urllib2.HTTPError as err:
        if err.code == 429:
            return 429
        if err.code == 404:
            return 404
        else:
            return None

    code = r.getcode()
    if code == 404:
        return 404
    elif code != 200:
        return None
    try:
        result = json.loads(decode_pagetext(r))
        if result.has_key("similar"):
            similar = result["similar"]
        else:
            similar = []
    except:
        return None

    return similar

#
# Normalize the name of the concept
#
def concept_name(cdata):
    cpath = cdata[0]
    cw = cdata[1]
    parts = [part for part in cpath.split("/") if len(part) > 0]
    # form of "/c/LANG/some_string" accepted
    if len(parts) == 3 and parts[0] == "c" and parts[1] == LANG:
        return parts[2], cw
    return None

# Download one concept with appropriate amount of delays and retries
# Error codes:
# 0 - No error
# 1 - MIT server thinks we're spamming too much
# 2 - Something is wrong, might have to abort
#
def fetch_tree(root_concept):
    randomized_pause() #throttle down
    while True:  # download with retries
        similar = cnet5_call(root_concept, 200)
        if similar == 429:
            if not handle_429():
                return 1, []
        else:
            break
    if similar is None:
        return 2, []
    elif similar == 404:
        return 0, []

    return 0, [ cdata for cdata in map(concept_name, similar)
        if cdata is not None ]

#
# Process the list of tags in the input file
# Input format:
# Tag,concept,...
#
def fetch_tagdata(f):
    out = {}
    while 1:
        l = f.readline()
        if not l:
            break
        ll = [x.strip() for x in l.split(",")]
        tag, concepts = ll[0], ll[1:]
        out[tag] = []
        for concept in concepts:
            err, tree = fetch_tree(concept) # tree is actually a flat list
            out[tag] += tree
            print (tag, concept, len(tree))
            if err:
                print "Stopping with error code %d"%(err)
                return out

    return out

def usage(prog):
    print "usage: %s <concepts file> <output file>"%(prog)
    sys.exit(2)

def die(msg):
    print msg
    sys.exit(1)

if __name__ == "__main__":
    import sys
    if len(sys.argv) < 3:
        usage(sys.argv[0])

    try:
        f = open(sys.argv[1], "r")
    except:
        die("Failed to open input file")
    try:
        ff = open(sys.argv[2], "w")
    except:
        die("Failed to open output file")
    out = fetch_tagdata(f)
    json.dump(out, ff, indent=2)

    f.close()
    ff.close()

