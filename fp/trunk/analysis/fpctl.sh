#!/bin/sh

# TODO:
# read database names, sizes, backup file locations from config file

# point to whitedb installation
export PYTHONPATH=/usr/local/lib/python2.7/site-packages
export PATH=$PATH:$HOME/Ms/fp/trunk/analysis

BACKUPDIR=./backup

# params: directory path
#
check_dir() {
  if [ ! -d ${1} ]; then
    mkdir -p ${1}
  fi
}

# params: database name, size, index column 
#
init_empty() {
  wgdb $1 info > /dev/null
  if [ "$?" = "0" ]; then
    echo "$1 already in memory, skipping initialization"
  else
    wgdb $1 create $2
    if [ "${3}X" != "X" ]; then
      wgdb $1 createindex $3 > /dev/null
    fi
  fi
}

# params: database name, size, index column 
#
load_or_init() {
  wgdb $1 info > /dev/null
  if [ "$?" = "0" ]; then
    echo "$1 already in memory, skipping initialization"
  else
    DUMP=${BACKUPDIR}/${1}.bin
    if [ -f ${DUMP} ]; then
      ERR=`wgdb $1 import ${DUMP} | grep "Import failed"`
      if [ "X${ERR}" != "X" ]; then
        echo "Import failed, initializing"
        init_empty $1 $2 $3
      fi
    else
      init_empty $1 $2 $3
    fi
  fi
}

# params: database name, filename, (optional) "gz"
export_dump() {
  check_dir ${BACKUPDIR}
  DUMP=${BACKUPDIR}/$2
  wgdb $1 export ${DUMP} > /dev/null
  if [ "${3}" = "gz" ]; then
    gzip ${DUMP}
  fi
}

# params: database name
#
dump_and_free() {
  export_dump $1 ${1}.bin
  wgdb $1 free
}

# params: database name
#
free_nodump() {
  wgdb $1 free
}

# params: database name
#
make_backup() {
  TIMESTAMP=`date +%Y%m%d%H%M`
  export_dump $1 ${1}-${TIMESTAMP}.bin gz
}

PROG=$0

usage() {
  echo "usage: ${PROG} start|stop|backup"
  exit 1
}

case "${1}" in
  start)
    load_or_init 101 10M 1
    init_empty 102 25M 2
    load_or_init 103 25M 1
    init_empty 104 10M
    exec python2 ./fp.py dummy.conf &
    ;;
  stop)
    PID=`pgrep -f 'fp\.py'`
    if [ "${PID}X" = "X" ]; then
      echo "Data analysis engine is not running"
    else
      kill ${PID}
      sleep 1
      dump_and_free 101
      free_nodump 102
      dump_and_free 103
      free_nodump 104
    fi
    ;;
  backup)
    make_backup 101
    make_backup 103
    ;;
  *)
    usage
    ;;
esac

