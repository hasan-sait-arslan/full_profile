#!/usr/bin/env python2
#
# Attach tags to a person

import whitedb
import sys
import math
import json
import log
import learn

import rowtype

#TAG_THRESHOLD=0.3
#SEMANTIC_THRESHOLD=0.05  # word relations below this confidence ignored

from aggrpers import get_aggr

#
# Load tag data
#
def get_tags(db, search_id):
    c = db.cursor()
    c.execute(arglist = [(0, whitedb.wgdb.COND_EQUAL, rowtype.TAG),
                        (2, whitedb.wgdb.COND_EQUAL, search_id)])
    pt = {}
    for rec in c.fetchall():
        p = rec[1]
        if not pt.has_key(p):
            pt[p] = {}
        pt[p][rec[3]] = rec[4]
    c.close()
    return pt

#
# Store tags for a person
#
def store_tag(db, p, search_id, t, P):
    try:
        db.insert([rowtype.TAG, p, search_id, t, P])
    except:
        return -1, "Insert failed"
    return 0, "OK"

#
# Rate keyword by page count, best distance and average distance
# page count and best distance have double weight
# returns values between 0..1
#
def kwrating(pc, b, a):
    c1 = 1-1.0/pc
    c2 = 1.0/math.log(b/10.0+math.e)
    c3 = 1.0/math.log(a/10.0+math.e)
    return (2*c1 + 2*c2 + c3)/5.0

#
# Calculate combined probability
# (formula used in spam filtering best practice)
# logarithm version to handle large vectors of low probabilities
#
def combine(pvect):
    eta = 0
    for pe in pvect:
        eta += math.log(1.0 - pe) - math.log(pe)
    return 1.0/(1+math.exp(eta))

#
# Compute tags and probabilities for persons
# returns dict { person : { tag, probability, ...}, ...}
#
def compute_tags(pa, tags, stats, ff):
    pt = {}
    for p,wd in pa.iteritems():
        ptags = {}
        for t,tkw in tags.iteritems():
            pvect = []
            maxpc = 0
            maxP = 0
            totP = 0
            wcnt = 0
            #for kw in tkw:
            for kw, sem_w in tkw:
                # XXX: this does not match multiword currently. This
                # requires a major reedsign
                kwstats = wd.get(kw)
                if kwstats is not None:
                    pc, b, a = kwstats
                    P = kwrating(pc, b, a) * sem_w
                    if pc > maxpc:
                        maxpc = pc
                    if P > maxP:
                        maxP = P
                    totP += P
                    wcnt += 1
            if wcnt > 0:
                avgP = totP / wcnt
                isrelated = learn.classify(t,
                   (p, t, maxpc, maxP, avgP, wcnt), stats)
                if isrelated:
                    if p not in pt.keys():
                        pt[p] = {}
                    pt[p][t] = 0.9 # claim high confidence, but not 1.0
                ff.write("%s,%s,%d,%f,%f,%d,%d\n"%(
                   p, t, maxpc, maxP, avgP, wcnt, int(isrelated)))
    return pt


#
# Parse the tags file
#
def load_tags(f):
    tags = {}
    cnet_data = json.load(f)
    cnt = 0
    skipped = 0
    for tag, cl in cnet_data.iteritems():
        tag = tag.encode("utf-8")
        tags[tag] = []
        for concept, w in cl:
            # XXX: may need an adjustment here. Currently pass as is
            norm_w = w
            cnt += 1
            tags[tag].append((concept.encode("utf-8"), norm_w))
    log.log("Loaded %d concepts (skipped %d below threshold)"%(cnt, skipped))
    return tags

#
# Parse the tags file: old versions
#
#def load_tags(f):
#    tags = {}
#    # New tags format
#    cnet_data = json.load(f)
#    # Find max weight
#    maxw = 0.0
#    for tag, cl in cnet_data.iteritems():
#        for concept, d, w in cl:
#            if w is not None and w > maxw:
#                maxw = w
#    log.log("Max weight from concepts file: %.2f"%(maxw))
#
#    # Follow the trees
#    # XXX: repeated words (due to occuring in phrases) should
#    # be aggregated here using combine() - currently this happens
#    # in the compute_tags() function which is inefficient
#    cnt = 0
#    skipped = 0
#    for tag, cl in cnet_data.iteritems():
#        tag = tag.encode("utf-8")
#        tags[tag] = []
#        curr_d = -1
#        path_w = []
#        for concept, d, w in cl:
#            if d == 0: # the root of the tree
#                norm_w = 1.0
#            else:
#                if w < 0:
#                    w = 0
#                norm_w = (w / maxw) * path_w[-1]
#            if norm_w < SEMANTIC_THRESHOLD:
#                skipped += 1
#            else:
#                cnt += 1
#                tags[tag].append((concept.encode("utf-8"), norm_w))
#            if d > curr_d:
#                # step deeper
#                path_w.append(norm_w)
#            elif d < curr_d:
#                # track back
#                path_w = path_w[:(d+1)]
#            curr_d = d
#    log.log("Loaded %d concepts (skipped %d below threshold)"%(cnt, skipped))
#
#    # Old tags format
##    for l in f.readlines():
##        ll = [x.strip() for x in l.split(",")]
##        tags[ll[0]] = ll[1:]
#    return tags

#
# attach tags to persons
#
def tag_pers(tags_db, f, ff, search_id):
    tags = load_tags(f)
    stats = json.load(ff)
    #print (tags,)
    pa = get_aggr(tags_db, search_id)
    fff = open("classif_stats.csv", "a") # hack, fix me
    pt = compute_tags(pa, tags, stats, fff)
    fff.close()
    for p, ptags in pt.iteritems():
        for t, P in ptags.iteritems():
            err, msg = store_tag(tags_db, p, search_id, t, P)
            if err:
                return err, msg
    return 0, "OK"

def usage(prog):
    print "usage: %s <tags file> <stats file> <search id>"%(prog)
    sys.exit(2)

def die(msg):
    print msg
    sys.exit(1)

if __name__ == "__main__":
    if len(sys.argv) < 4:
        usage(sys.argv[0])

    try:
        search_id = int(sys.argv[3])
    except:
        die("Search id should be a number")

    try:
        tags_db = whitedb.connect("104")
    except:
        die("Failed to attach to database")

    try:
        f = open(sys.argv[1], "r")
    except:
        f = None
        err, msg = -1, "Cannot open tags file"

    try:
        ff = open(sys.argv[2], "r")
    except:
        ff = None
        err, msg = -1, "Cannot open stats file"

    if f and ff:
        err, msg = tag_pers(tags_db, f, ff, search_id)
    if f:
        f.close()
    if ff:
        ff.close()

    if err:
        print "Error: "+msg
    else:
        print "OK"

    tags_db.close()
