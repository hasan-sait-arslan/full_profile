#!/usr/bin/env python2
# -*- coding: UTF-8 -*-
#


kill_list = set(["&#160", "the", "was", "that", "and", "you", "&nbsp",
  "~14", "~12", "~11", "&#107&#114", "ning", "for", "see", "with",
  "kes", "-->", "mis", "oli", "this", "kui", "&#9658", "aga", "//]]>",
  "oma", "juba", "veel", "//-->", "&quot", "&uuml", "|&nbsp", "+00",
  "&lt", "10px", "selle", "või", "mida", "nagu", "üle", "sel",
  "kas", "siis", "sest", "eest", "kuidas", "jquery", "css",
  "k&otilde", "v&otilde", "nii", "mitte", "enne", "väga", "pole",
  "more", "new", "profile", "view", "&amp", "name", "your", "from",
  "not", "get", "people", "who", "all", "full", "are", "find",
  "com", "join", "first", "about", "contact", "search", "have",
  "also", "function", "var", "know", "area", "content", "our", "has",
  "looking", "main", "named", "show", "http", "below", "can", "window",
  "skip", "one", "page", "out", "what", "else", "&#8211", "will",
  "other", "now", "like", "&raquo", "sisse", "but", "log", "list",
  "any", "time", "when", "how", "their", "last", "{var", "null",
  "undefined", "{if", "//www", "–", "just", "they", "add", "been",
  "{return", "false", "true", "url", "most", "after", "back", "try"])

#
# Return true, if a word is expected to carry information
# otherwise return false.
#
def not_stopword(w):
    if len(w) < 3: return False
    if w.lower() in kill_list: return False
    return True
