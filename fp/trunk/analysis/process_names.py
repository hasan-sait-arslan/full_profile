#!/usr/bin/python
# -*- coding: utf-8 -*-

import time, signal, os, traceback, logging, logging.handlers
import urllib2, socket, gzip, json
from operator import itemgetter

girls=[]
boys=[]
sur=[]

def firstnames(fname,sex):
  global girls,boys
  f=open(fname,"r")
  data=f.readlines()
  f.close()
  for el in data:
    s=el.strip()
    if not s: continue
    sp=s.split()
    if len(sp)<2: continue
    row=[sp[0].strip().lower(),float(sp[1]),sex]
    if sex=="f": girls.append(row)
    else: boys.append(row)
    
def surnames(fname):
  global girls,boys,sur
  f=open(fname,"r")
  data=f.readlines()
  f.close()
  for el in data:
    s=el.strip()
    if not s: continue
    sp=s.split()
    if len(sp)<4: continue
    row=[sp[1].strip().lower(),int(sp[2])*1000+int(sp[3])]
    """
    try:
      row=[sp[1].strip().lower(),int(sp[2])*1000+int(sp[3])]
    except:
      print "*********",row      
    """  
    sur.append(row)    
    
#1.	Smith	729 862

def merge():
  global girls,boys,sur
  tmp=girls+boys
  r=sorted(tmp,key=itemgetter(1),reverse=True)
  print "firstnames=["
  for el in r:    
    print str(el)+","
  print "]"
  print "surnames=["
  for el in sur:    
    print str(el)+","
  print "]" 

firstnames("girls.txt","f")
firstnames("boys.txt","m")
surnames("surnames.txt")
merge()
