#!/usr/bin/env python2
#

errlog = None
msglog = None

MSG = 1
ERR = 2

#
# Write a message to the appropriate stream
#
def log(msg, msgcl = MSG):
    if msgcl == MSG and msglog is not None:
        msglog.write(msg+"\n")
        msglog.flush()
    elif msgcl == ERR and errlog is not None:
        errlog.write(msg+"\n")
        errlog.flush()

