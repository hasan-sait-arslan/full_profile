#!/usr/bin/env python
#
# Google search (cached in whitedb)

import urllib2
from base64 import b64encode
import json


BINGAPI_URL= "https://api.datamarket.azure.com/Bing/Search/"
bing_key = None

import urllib2

def bingapi_uri(q, top=10, realm="Web"):
    return "".join([BINGAPI_URL,
        realm,
        "?Query=",
        urllib2.quote("'"+q+"'"),
        "&$top=",
        str(top),
        "&$format=json"])

#
# Run a Bing search query
# returns a list of matching urls
# XXX: skip ignored for now
#
def bingapi_call(q, top=10, skip = 0):
    request = urllib2.Request(bingapi_uri(q, top))
    request.add_header('Authorization',
        'Basic ' + b64encode(bing_key + ':' + bing_key))
    r = urllib2.urlopen(request)

    if r.getcode() != 200:
        return None
    try:
        result = json.load(r)
        if result.has_key('d'):
            resrows = result['d']['results']
        else:
            resrows = []
    except:
        return None

    urls = []
    for idx, row in enumerate(resrows):
        url = [row.get('Url'),
            row.get('Title') or u'',
            row.get('Description') or u'']
        if url[0]:
            urls.append(tuple([x.encode('utf-8') for x in url]) + (idx,))

    return urls

#
# Do a Bing web search; one page
#
def bing_websearch(q, top=10):
    if bing_key is None:
        return None
    return bingapi_call(q, top)

